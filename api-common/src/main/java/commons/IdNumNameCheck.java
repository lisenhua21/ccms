package commons;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
//import jdk.nashorn.internal.ir.RuntimeNode;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import utils.HttpUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 检查身份证.手机与姓名
 * </p>
 *
 * @author liso
 * @since 2020/9/29 20:53
 */
public class IdNumNameCheck {
//    这是参考网址
//    https://market.aliyun.com/products/57000002/cmapi00043147.html?spm=5176.2020520132.101.14.5d867218fkr27N
// #product-contain

    /**
    *
    * @author liso
    * @since 2020/9/30 10:28
    * * @param idnum 身份证
    * * @param mobilnum 手机号
    * * @param name 真实姓名
    * * @return null
    */
    public static boolean check(String idnum, String mobilnum, String name) {
        String host = "https://lfysjrz3.market.alicloudapi.com";
        String path = "/verify/amobileverify3jian";
        String method = "GET";
        String appcode = "da39d4344f054915bfd39e340dce3770";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("certcode", idnum);
        querys.put("mobile", mobilnum);
        querys.put("name", name);


        try {

            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);

            System.out.println(response.toString());
            //获取response的body
            String x = EntityUtils.toString(response.getEntity(), "UTF-8");

            //              这是返回的数据格式
            //            {
            //                "code": 0,
            //                "message": "校验完成",
            //                "data": {
            //                          "result": 1,
            //                          "remark": "一致"
            //            }
            //            }

            JSONObject object = (JSONObject) JSONObject.parse(x);
            Object code = object.get("code");
            Object message = object.get("message");
            Object data = object.get("data");
            JSONObject o = (JSONObject) JSON.toJSON(data);
            Object result = o.get("result");
            Object remark = o.get("remark");
            //  目前只取一个布尔值足够了 如果有其他返回值再修改一下方法也能轻松使用
            if (Integer.parseInt(code.toString()) == 0) {
                return true;
            }
//            System.out.println(x);
//
//            System.out.println(code);
//            System.out.println(message);
//
//            System.out.println(result);
//            System.out.println(remark);


            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
////测试
//    public static void main(String[] args) {
//        check("4418261x","15350703352","李彬森");
//    }

}
