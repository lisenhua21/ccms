package commons;

/**
 * layui回显的数据的Result对象
 */

public class LayuiResult {

    private Integer code = 0;
    private String msg = "";
    private Long count;
    private Object data;

    public LayuiResult() {
    }

    public LayuiResult(Long count, Object data) {
        this.count = count;
        this.data = data;
    }

    public LayuiResult(Object data) {
        this.data = data;
    }

    public LayuiResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public LayuiResult(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 出现错误的回显
     *
     * @param code
     * @param msg
     * @return
     */
    public static LayuiResult success(Integer code, String msg) {
        return new LayuiResult(code, msg);
    }

    /**
     * 出现错误的回显
     *
     * @param code
     * @param msg
     * @return
     */
    public static LayuiResult fail(Integer code, String msg) {
        return new LayuiResult(code, msg);
    }

    /**
     * layui所需数据格式
     *
     * @param count
     * @param data
     * @return
     */
    public static LayuiResult layData(Long count, Object data) {
        return new LayuiResult(count, data);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "LayuiResult{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", count=" + count +
                ", data=" + data +
                '}';
    }
}
