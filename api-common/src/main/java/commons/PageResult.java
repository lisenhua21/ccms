package commons;

import java.util.List;

public class PageResult<T> {
    private Integer pageIndex;  //当前页
    private Integer pageSize;   //页面条数
    private Long totalNum;      //总条数
    private Integer totalPage;  //总页数
    private List<T> datas;      //当前页数据

    public PageResult() {
    }

    public PageResult(Integer pageIndex, Integer pageSize, Long totalNum, Integer totalPage, List<T> datas) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.totalNum = totalNum;
        this.totalPage = totalPage;
        this.datas = datas;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Long totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    @Override
    public String toString() {
        return "PageResult{" +
                "pageIndex=" + pageIndex +
                ", pageSize=" + pageSize +
                ", totalNum=" + totalNum +
                ", totalPage=" + totalPage +
                ", datas=" + datas +
                '}';
    }
}