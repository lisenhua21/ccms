package entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
@Table(name = "sys_admin")
public class Admin implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 管理员ID,自增
     */
    @Id
    private Integer id;

    /**
     * 管理员昵称
     */
    private String nickName;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别，0为男，1为女
     */
    private Integer sex;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否可用
     */
    private Integer available;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 用户类型[0,超级管理员;1,管理员;2普通用户]
     */
    private Integer type;

    /**
     * 头像地址
     */
    private String img;

    /**
     * 盐
     */
    private String salt;

    public Admin() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", nickName='" + nickName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", sex=" + sex +
                ", remark='" + remark + '\'' +
                ", available=" + available +
                ", orderNum=" + orderNum +
                ", type=" + type +
                ", img='" + img + '\'' +
                ", salt='" + salt + '\'' +
                '}';
    }
}
