package entity;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
@Table(name = "sys_admin_role")
public class AdminRole implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer aid;

    private Integer rid;

    public AdminRole() {
    }

    public AdminRole(Integer aid, Integer rid) {
        this.aid = aid;
        this.rid = rid;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    @Override
    public String toString() {
        return "AdminRole{" +
                "aid=" + aid +
                ", rid=" + rid +
                '}';
    }
}
