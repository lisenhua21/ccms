package entity;

public class BillList {
    private Integer billid;         //账单ID
    private String billcreate;        //账单生成日期
    private Double billmoney;       //账单总金额
    private String billstatus;      //账单状态(未完成，已完成，已逾期)
    private String billcomplete;      //账单完成日期
    private String cardnum;        //信用卡号
    private String billrepaytime;         //账单还款日期
    private String billproduce;     //账单通知方式

    public BillList() {
    }

    public BillList(Integer billid, String billcreate, Double billmoney, String billstatus, String billcomplete, String cardnum, String billrepaytime, String billproduce) {
        this.billid = billid;
        this.billcreate = billcreate;
        this.billmoney = billmoney;
        this.billstatus = billstatus;
        this.billcomplete = billcomplete;
        this.cardnum = cardnum;
        this.billrepaytime = billrepaytime;
        this.billproduce = billproduce;
    }

    public Integer getBillid() {
        return billid;
    }

    public void setBillid(Integer billid) {
        this.billid = billid;
    }

    public String getBillcreate() {
        return billcreate;
    }

    public void setBillcreate(String billcreate) {
        this.billcreate = billcreate;
    }

    public Double getBillmoney() {
        return billmoney;
    }

    public void setBillmoney(Double billmoney) {
        this.billmoney = billmoney;
    }

    public String getBillstatus() {
        return billstatus;
    }

    public void setBillstatus(String billstatus) {
        this.billstatus = billstatus;
    }

    public String getBillcomplete() {
        return billcomplete;
    }

    public void setBillcomplete(String billcomplete) {
        this.billcomplete = billcomplete;
    }

    public String getCardnum() {
        return cardnum;
    }

    public void setCardnum(String cardnum) {
        this.cardnum = cardnum;
    }

    public String getBillrepaytime() {
        return billrepaytime;
    }

    public void setBillrepaytime(String billrepaytime) {
        this.billrepaytime = billrepaytime;
    }

    public String getBillproduce() {
        return billproduce;
    }

    public void setBillproduce(String billproduce) {
        this.billproduce = billproduce;
    }

    @Override
    public String toString() {
        return "BillList{" +
                "billid=" + billid +
                ", billcreate='" + billcreate + '\'' +
                ", billmoney=" + billmoney +
                ", billstatus='" + billstatus + '\'' +
                ", billcomplete='" + billcomplete + '\'' +
                ", cardnum='" + cardnum + '\'' +
                ", billrepaytime='" + billrepaytime + '\'' +
                ", billproduce='" + billproduce + '\'' +
                '}';
    }
}
