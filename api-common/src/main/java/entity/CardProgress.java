package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * 申请表(信用卡)
 */
@Table(name = "ccms_cardprogress")
public class CardProgress {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer cardProgressId;     //办卡申请表id
    private String cpName;              //申请表名
    private String creditType;          //办卡类型
    private Integer essentialid;        //基本信息id
    private Integer uid;                //单位信息id
    private Integer contactId;          //联系人信息id
    private String cpStatus;            //申请状态
    private String cpReason;            //原因(不通过时填写)
    private Date cpCreateTime;          //创建时间
    private Integer cpCreateUserid;     //申请人
    private Date cpDealTime;            //审核时间
    private Integer cpDealAdmin;        //审核人
    @Transient
    private String adminName;           //审核人名称

    public CardProgress() {
    }

    public CardProgress(Integer cardProgressId, String cpName, String creditType, Integer essentialid, Integer uid, Integer contactId, String cpStatus, String cpReason, Date cpCreateTime, Integer cpCreateUserid, Date cpDealTime, Integer cpDealAdmin) {
        this.cardProgressId = cardProgressId;
        this.cpName = cpName;
        this.creditType = creditType;
        this.essentialid = essentialid;
        this.uid = uid;
        this.contactId = contactId;
        this.cpStatus = cpStatus;
        this.cpReason = cpReason;
        this.cpCreateTime = cpCreateTime;
        this.cpCreateUserid = cpCreateUserid;
        this.cpDealTime = cpDealTime;
        this.cpDealAdmin = cpDealAdmin;
    }

    public Integer getCardProgressId() {
        return cardProgressId;
    }

    public void setCardProgressId(Integer cardProgressId) {
        this.cardProgressId = cardProgressId;
    }

    public String getCpName() {
        return cpName;
    }

    public void setCpName(String cpName) {
        this.cpName = cpName;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public Integer getEssentialid() {
        return essentialid;
    }

    public void setEssentialid(Integer essentialid) {
        this.essentialid = essentialid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public String getCpStatus() {
        return cpStatus;
    }

    public void setCpStatus(String cpStatus) {
        this.cpStatus = cpStatus;
    }

    public String getCpReason() {
        return cpReason;
    }

    public void setCpReason(String cpReason) {
        this.cpReason = cpReason;
    }

    public Date getCpCreateTime() {
        return cpCreateTime;
    }

    public void setCpCreateTime(Date cpCreateTime) {
        this.cpCreateTime = cpCreateTime;
    }

    public Integer getCpCreateUserid() {
        return cpCreateUserid;
    }

    public void setCpCreateUserid(Integer cpCreateUserid) {
        this.cpCreateUserid = cpCreateUserid;
    }

    public Date getCpDealTime() {
        return cpDealTime;
    }

    public void setCpDealTime(Date cpDealTime) {
        this.cpDealTime = cpDealTime;
    }

    public Integer getCpDealAdmin() {
        return cpDealAdmin;
    }

    public void setCpDealAdmin(Integer cpDealAdmin) {
        this.cpDealAdmin = cpDealAdmin;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    @Override
    public String toString() {
        return "CardProgress{" +
                "cardProgressId=" + cardProgressId +
                ", cpName='" + cpName + '\'' +
                ", creditType='" + creditType + '\'' +
                ", essentialid=" + essentialid +
                ", uid=" + uid +
                ", contactId=" + contactId +
                ", cpStatus='" + cpStatus + '\'' +
                ", cpReason='" + cpReason + '\'' +
                ", cpCreateTime=" + cpCreateTime +
                ", cpCreateUserid=" + cpCreateUserid +
                ", cpDealTime=" + cpDealTime +
                ", cpDealAdmin=" + cpDealAdmin +
                '}';
    }
}
