package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
//银行卡
@Table(name = "ccms_bankcard")
public class CcmsBankcard {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer bankcardid;
    private String bankcardnum;
    private String phonenum;
    private BigDecimal balance;
    private String password;
    private Integer userId;
    public CcmsBankcard() {
    }

    public CcmsBankcard(Integer bankcardid, String bankcardnum, String phonenum, BigDecimal balance, String password, Integer userId) {
        this.bankcardid = bankcardid;
        this.bankcardnum = bankcardnum;
        this.phonenum = phonenum;
        this.balance = balance;
        this.password = password;
        this.userId = userId;
    }

    public Integer getBankcardid() {
        return bankcardid;
    }

    public void setBankcardid(Integer bankcardid) {
        this.bankcardid = bankcardid;
    }

    public String getBankcardnum() {
        return bankcardnum;
    }

    public void setBankcardnum(String bankcardnum) {
        this.bankcardnum = bankcardnum;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "CcmsBankcard{" +
                "bankcardid=" + bankcardid +
                ", bankcardnum='" + bankcardnum + '\'' +
                ", phonenum='" + phonenum + '\'' +
                ", balance=" + balance +
                ", password='" + password + '\'' +
                ", userId=" + userId +
                '}';
    }
}
