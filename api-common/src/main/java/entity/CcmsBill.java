package entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

@Table
public class CcmsBill {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer billId;         //账单ID
    private String billCreate;        //账单生成日期
    private Double billStage;       //本月应还分期金额
    private Double billAmount;      //上月本卡已消费金额
    private String billStatus;      //账单状态(未完成，已完成，已逾期)
    private String billComplete;      //账单完成日期
    private String billType;        //账单类型(一次付清，分3期，分6期，分12期)
    private Integer creditId;        //信用卡ID
    private String billRepaytime;         //账单还款日期
    private String billProduce;     //账单材质

    public CcmsBill() {
    }

    public CcmsBill(Integer billId, String billCreate) {
        this.billId = billId;
        this.billCreate = billCreate;
    }

    public CcmsBill(Integer billId, String billCreate, Double billStage, Double billAmount, String billStatus, String billComplete, String billType, Integer creditId, String billRepaytime, String billProduce) {
        this.billId = billId;
        this.billCreate = billCreate;
        this.billStage = billStage;
        this.billAmount = billAmount;
        this.billStatus = billStatus;
        this.billComplete = billComplete;
        this.billType = billType;
        this.creditId = creditId;
        this.billRepaytime = billRepaytime;
        this.billProduce = billProduce;
    }

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public String getBillCreate() {
        return billCreate;
    }

    public void setBillCreate(String billCreate) {
        this.billCreate = billCreate;
    }

    public Double getBillStage() {
        return billStage;
    }

    public void setBillStage(Double billStage) {
        this.billStage = billStage;
    }

    public Double getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(Double billAmount) {
        this.billAmount = billAmount;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getBillComplete() {
        return billComplete;
    }

    public void setBillComplete(String billComplete) {
        this.billComplete = billComplete;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public Integer getCreditId() {
        return creditId;
    }

    public void setCreditId(Integer creditId) {
        this.creditId = creditId;
    }

    public String getBillRepaytime() {
        return billRepaytime;
    }

    public void setBillRepaytime(String billRepaytime) {
        this.billRepaytime = billRepaytime;
    }

    public String getBillProduce() {
        return billProduce;
    }

    public void setBillProduce(String billProduce) {
        this.billProduce = billProduce;
    }

    @Override
    public String toString() {
        return "CcmsBill{" +
                "billId=" + billId +
                ", billCreate='" + billCreate + '\'' +
                ", billStage=" + billStage +
                ", billAmount=" + billAmount +
                ", billStatus='" + billStatus + '\'' +
                ", billComplete='" + billComplete + '\'' +
                ", billType='" + billType + '\'' +
                ", creditId=" + creditId +
                ", billRepaytime='" + billRepaytime + '\'' +
                ", billProduce='" + billProduce + '\'' +
                '}';
    }
}
