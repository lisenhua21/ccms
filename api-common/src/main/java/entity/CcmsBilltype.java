package entity;

import javax.persistence.Id;
import javax.persistence.Table;

@Table
public class CcmsBilltype {
    @Id
    private String typeid;
    private String typename;

    public CcmsBilltype() {
    }

    public CcmsBilltype(String typeid, String typename) {
        this.typeid = typeid;
        this.typename = typename;
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    @Override
    public String toString() {
        return "CcmsBilltype{" +
                "typeid='" + typeid + '\'' +
                ", typename='" + typename + '\'' +
                '}';
    }
}
