package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

/**
*
* <p>
* 城市表
* </p>
* @author liso
* @since 2020/9/27 20:45
*/
@Table(name = "ccms_city")
public class CcmsCity {
    /**
     * 城市id
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer cityid;

    private Integer fid;

    private String city;

    /**
     * Ccms city
     */
    public CcmsCity() {
    }

    /**
     * Ccms city
     *
     * @param cityid cityid
     * @param fid    fid
     * @param city   city
     */
    public CcmsCity(Integer cityid, Integer fid, String city) {
        this.cityid = cityid;
        this.fid = fid;
        this.city = city;
    }

    /**
     * Gets cityid *
     *
     * @return the cityid
     */
    public Integer getCityid() {
        return cityid;
    }

    /**
     * Sets cityid *
     *
     * @param cityid cityid
     */
    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }

    /**
     * Gets fid *
     *
     * @return the fid
     */
    public Integer getFid() {
        return fid;
    }

    /**
     * Sets fid *
     *
     * @param fid fid
     */
    public void setFid(Integer fid) {
        this.fid = fid;
    }

    /**
     * Gets city *
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city *
     *
     * @param city city
     */
    public void setCity(String city) {
        this.city = city;
    }
}