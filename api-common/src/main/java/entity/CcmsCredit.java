package entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.crypto.Data;
import java.math.BigDecimal;

@Table(name = "ccms_creditcard")
public class CcmsCredit {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer creditId;       //信用卡id
    @Column(name = "cardnum")
    private long cardnum;       //信用卡号
    @Column(name = "credit_type")
    private String creditType;        //信用卡类型
    @Column(name = "active")
    private String active;          //是否激活
    @Column(name = "status")
    private String  status;          //状态
    @Column(name = "credit_limit")
    private BigDecimal creditLimit;         //信用卡额度
    @Column(name = "debt")
    private BigDecimal debt;                //当前欠款
    @Column(name = "deposit")
    private BigDecimal deposit;             //预存款
    @Column(name="essentialId")
    private int essentialId;                //用户基本信息ID
    @Column(name="credit_pwd")
    private String creditPwd;                  //卡密码
    @Column(name="remind_time")
    private String remindTime;                // 提醒还款时间
    @Column(name="user_id")
    private Integer userId;                  //卡所属用户Id
    @Column(name="repayment")
    private String repayment;               //本期到账还款日

    public CcmsCredit() {
    }

    public CcmsCredit(Integer creditId, long cardnum, String creditType, String active, String status, BigDecimal creditLimit, BigDecimal debt, BigDecimal deposit, int essentialId, String creditPwd, String remindTime, Integer userId, String repayment) {
        this.creditId = creditId;
        this.cardnum = cardnum;
        this.creditType = creditType;
        this.active = active;
        this.status = status;
        this.creditLimit = creditLimit;
        this.debt = debt;
        this.deposit = deposit;
        this.essentialId = essentialId;
        this.creditPwd = creditPwd;
        this.remindTime = remindTime;
        this.userId = userId;
        this.repayment = repayment;
    }

    public Integer getCreditId() {
        return creditId;
    }

    public void setCreditId(Integer creditId) {
        this.creditId = creditId;
    }

    public long getCardnum() {
        return cardnum;
    }

    public void setCardnum(long cardnum) {
        this.cardnum = cardnum;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public BigDecimal getDebt() {
        return debt;
    }

    public void setDebt(BigDecimal debt) {
        this.debt = debt;
    }

    public BigDecimal getDeposit() {
        return deposit;
    }

    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }

    public int getEssentialId() {
        return essentialId;
    }

    public void setEssentialId(int essentialId) {
        this.essentialId = essentialId;
    }

    public String getCreditPwd() {
        return creditPwd;
    }

    public void setCreditPwd(String creditPwd) {
        this.creditPwd = creditPwd;
    }

    public String getRemindTime() {
        return remindTime;
    }

    public void setRemindTime(String remindTime) {
        this.remindTime = remindTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    @Override
    public String toString() {
        return "CcmsCredit{" +
                "creditId=" + creditId +
                ", cardnum=" + cardnum +
                ", creditType='" + creditType + '\'' +
                ", active='" + active + '\'' +
                ", status='" + status + '\'' +
                ", creditLimit=" + creditLimit +
                ", debt=" + debt +
                ", deposit=" + deposit +
                ", essentialId=" + essentialId +
                ", creditPwd=" + creditPwd +
                ", remindTime='" + remindTime + '\'' +
                ", userId=" + userId +
                ", repayment='" + repayment + '\'' +
                '}';
    }
}
