package entity;

import javax.persistence.Id;
import javax.persistence.Table;

@Table
public class CcmsCreditcardtype {
    @Id
    private String typeid;           //信用卡类型ID
    private String typename;         //类型名
    private String applyStandard;    //申请门槛
    private Long cardLimit;          //该类型卡的当月额度
    private String threeStage;       //分期利率3
    private String sixStage;         //分期利率6
    private String nineStage;        //分期利率9
    private String twelveStage;      //分期利率12
    private String eighteenStage;    //分期利率18
    private String twentyfourStage;  //分期利率24
    private String creditcardImg;    //不同类型的卡面

    public CcmsCreditcardtype() {
    }

    public CcmsCreditcardtype(String typeid, String typename, String applyStandard, Long cardLimit, String threeStage, String sixStage, String nineStage, String twelveStage, String eighteenStage, String twentyfourStage, String creditcardImg) {
        this.typeid = typeid;
        this.typename = typename;
        this.applyStandard = applyStandard;
        this.cardLimit = cardLimit;
        this.threeStage = threeStage;
        this.sixStage = sixStage;
        this.nineStage = nineStage;
        this.twelveStage = twelveStage;
        this.eighteenStage = eighteenStage;
        this.twentyfourStage = twentyfourStage;
        this.creditcardImg = creditcardImg;
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getApplyStandard() {
        return applyStandard;
    }

    public void setApplyStandard(String applyStandard) {
        this.applyStandard = applyStandard;
    }

    public Long getCardLimit() {
        return cardLimit;
    }

    public void setCardLimit(Long cardLimit) {
        this.cardLimit = cardLimit;
    }

    public String getThreeStage() {
        return threeStage;
    }

    public void setThreeStage(String threeStage) {
        this.threeStage = threeStage;
    }

    public String getSixStage() {
        return sixStage;
    }

    public void setSixStage(String sixStage) {
        this.sixStage = sixStage;
    }

    public String getNineStage() {
        return nineStage;
    }

    public void setNineStage(String nineStage) {
        this.nineStage = nineStage;
    }

    public String getTwelveStage() {
        return twelveStage;
    }

    public void setTwelveStage(String twelveStage) {
        this.twelveStage = twelveStage;
    }

    public String getEighteenStage() {
        return eighteenStage;
    }

    public void setEighteenStage(String eighteenStage) {
        this.eighteenStage = eighteenStage;
    }

    public String getTwentyfourStage() {
        return twentyfourStage;
    }

    public void setTwentyfourStage(String twentyfourStage) {
        this.twentyfourStage = twentyfourStage;
    }

    public String getCreditcardImg() {
        return creditcardImg;
    }

    public void setCreditcardImg(String creditcardImg) {
        this.creditcardImg = creditcardImg;
    }

    @Override
    public String toString() {
        return "CcmsCreditcardtype{" +
                "typeid='" + typeid + '\'' +
                ", typename='" + typename + '\'' +
                ", applyStandard='" + applyStandard + '\'' +
                ", cardLimit=" + cardLimit +
                ", threeStage='" + threeStage + '\'' +
                ", sixStage='" + sixStage + '\'' +
                ", nineStage='" + nineStage + '\'' +
                ", twelveStage='" + twelveStage + '\'' +
                ", eighteenStage='" + eighteenStage + '\'' +
                ", twentyfourStage='" + twentyfourStage + '\'' +
                ", creditcardImg='" + creditcardImg + '\'' +
                '}';
    }
}
