package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "ccms_installment")
public class CcmsInstallment {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer installmentId;          //这是分期付款申请表id
    private Integer billId;                 //这是所属账单id
    private Double installmentMoney;        //用户要分期的金额
    private Integer installmentDateTime;    //这是用户分期的次数,分几期
    private Double interest;                //分期的利率
    private String applyDate;                        //申请时间
    private Double firstMonth;                     //首月应还分期
    private Double laterMonth;                     //其他月份的分期金额

    @Override
    public String toString() {
        return "CcmsInstallment{" +
                "installmentId=" + installmentId +
                ", billId=" + billId +
                ", installmentMoney=" + installmentMoney +
                ", installmentDateTime=" + installmentDateTime +
                ", interest=" + interest +
                ", applyDate=" + applyDate +
                ", firstMonth=" + firstMonth +
                ", laterMonth=" + laterMonth +
                '}';
    }

    public Integer getInstallmentId() {
        return installmentId;
    }

    public void setInstallmentId(Integer installmentId) {
        this.installmentId = installmentId;
    }

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public Double getInstallmentMoney() {
        return installmentMoney;
    }

    public void setInstallmentMoney(Double installmentMoney) {
        this.installmentMoney = installmentMoney;
    }

    public Integer getInstallmentDateTime() {
        return installmentDateTime;
    }

    public void setInstallmentDateTime(Integer installmentDateTime) {
        this.installmentDateTime = installmentDateTime;
    }

    public Double getInterest() {
        return interest;
    }

    public void setInterest(Double interest) {
        this.interest = interest;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public Double getFirstMonth() {
        return firstMonth;
    }

    public void setFirstMonth(Double firstMonth) {
        this.firstMonth = firstMonth;
    }

    public Double getLaterMonth() {
        return laterMonth;
    }

    public void setLaterMonth(Double laterMonth) {
        this.laterMonth = laterMonth;
    }

    public CcmsInstallment() {
    }

    public CcmsInstallment(Integer installmentId, Integer billId, Double installmentMoney, Integer installmentDateTime, Double interest, String applyDate, Double firstMonth, Double laterMonth) {
        this.installmentId = installmentId;
        this.billId = billId;
        this.installmentMoney = installmentMoney;
        this.installmentDateTime = installmentDateTime;
        this.interest = interest;
        this.applyDate = applyDate;
        this.firstMonth = firstMonth;
        this.laterMonth = laterMonth;
    }
}
