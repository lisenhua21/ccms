package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

//还款明细
@Table(name = "ccms_repayment")
public class CcmsRepayment {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer repaymentId;
    private String cardnum;
    private BigDecimal newBalance;
    private BigDecimal repayment;
    private String bankcardnum;
    private Date repaymentDate;
    private String repaymentState;
    private Integer billId;
    private Integer userId;

    public CcmsRepayment() {
    }

    public CcmsRepayment(Integer repaymentId, String cardnum, BigDecimal newBalance, BigDecimal repayment, String bankcardnum, Date repaymentDate, String repaymentState, Integer billId, Integer userId) {
        this.repaymentId = repaymentId;
        this.cardnum = cardnum;
        this.newBalance = newBalance;
        this.repayment = repayment;
        this.bankcardnum = bankcardnum;
        this.repaymentDate = repaymentDate;
        this.repaymentState = repaymentState;
        this.billId = billId;
        this.userId = userId;
    }

    public Integer getRepaymentId() {
        return repaymentId;
    }

    public void setRepaymentId(Integer repaymentId) {
        this.repaymentId = repaymentId;
    }

    public String getCardnum() {
        return cardnum;
    }

    public void setCardnum(String cardnum) {
        this.cardnum = cardnum;
    }

    public BigDecimal getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(BigDecimal newBalance) {
        this.newBalance = newBalance;
    }

    public BigDecimal getRepayment() {
        return repayment;
    }

    public void setRepayment(BigDecimal repayment) {
        this.repayment = repayment;
    }

    public String getBankcardnum() {
        return bankcardnum;
    }

    public void setBankcardnum(String bankcardnum) {
        this.bankcardnum = bankcardnum;
    }

    public Date getRepaymentDate() {
        return repaymentDate;
    }

    public void setRepaymentDate(Date repaymentDate) {
        this.repaymentDate = repaymentDate;
    }

    public String getRepaymentState() {
        return repaymentState;
    }

    public void setRepaymentState(String repaymentState) {
        this.repaymentState = repaymentState;
    }

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "CcmsRepayment{" +
                "repaymentId=" + repaymentId +
                ", cardnum='" + cardnum + '\'' +
                ", newBalance=" + newBalance +
                ", repayment=" + repayment +
                ", bankcardnum='" + bankcardnum + '\'' +
                ", repaymentDate=" + repaymentDate +
                ", repaymentState='" + repaymentState + '\'' +
                ", billId=" + billId +
                ", userId=" + userId +
                '}';
    }
}
