package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;

public class CcmsStage {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer stageId;//分期账单ID
    private String  stageNumber;//分期账单期数
    private Double  stageMoney;//分期账单本期应还金额
    private String  stageStatus;//分期账单状态
    private String  stageDate;//分期账单还款时间
    private String  stageRemark;//分期账单摘要
    private Integer billId;//分期账单所属总账单ID
    private Integer ccmsInstallmentId; //分期信息表id

    @Override
    public String toString() {
        return "CcmsStage{" +
                "stageId=" + stageId +
                ", stageNumber='" + stageNumber + '\'' +
                ", stageMoney=" + stageMoney +
                ", stageStatus='" + stageStatus + '\'' +
                ", stageDate='" + stageDate + '\'' +
                ", stageRemark='" + stageRemark + '\'' +
                ", billId=" + billId +
                ", ccmsInstallmentId=" + ccmsInstallmentId +
                '}';
    }

    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    public String getStageNumber() {
        return stageNumber;
    }

    public void setStageNumber(String stageNumber) {
        this.stageNumber = stageNumber;
    }

    public Double getStageMoney() {
        return stageMoney;
    }

    public void setStageMoney(Double stageMoney) {
        this.stageMoney = stageMoney;
    }

    public String getStageStatus() {
        return stageStatus;
    }

    public void setStageStatus(String stageStatus) {
        this.stageStatus = stageStatus;
    }

    public String getStageDate() {
        return stageDate;
    }

    public void setStageDate(String stageDate) {
        this.stageDate = stageDate;
    }

    public String getStageRemark() {
        return stageRemark;
    }

    public void setStageRemark(String stageRemark) {
        this.stageRemark = stageRemark;
    }

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public Integer getCcmsInstallmentId() {
        return ccmsInstallmentId;
    }

    public void setCcmsInstallmentId(Integer ccmsInstallmentId) {
        this.ccmsInstallmentId = ccmsInstallmentId;
    }

    public CcmsStage() {
    }

    public CcmsStage(Integer stageId, String stageNumber, Double stageMoney, String stageStatus, String stageDate, String stageRemark, Integer billId, Integer ccmsInstallmentId) {
        this.stageId = stageId;
        this.stageNumber = stageNumber;
        this.stageMoney = stageMoney;
        this.stageStatus = stageStatus;
        this.stageDate = stageDate;
        this.stageRemark = stageRemark;
        this.billId = billId;
        this.ccmsInstallmentId = ccmsInstallmentId;
    }
    public CcmsStage(String stageNumber, Double stageMoney, String stageStatus, String stageDate, String stageRemark, Integer billId, Integer ccmsInstallmentId) {
        this.stageNumber = stageNumber;
        this.stageMoney = stageMoney;
        this.stageStatus = stageStatus;
        this.stageDate = stageDate;
        this.stageRemark = stageRemark;
        this.billId = billId;
        this.ccmsInstallmentId = ccmsInstallmentId;
    }
}
