package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "ccms_user_info")
public class CcmsUser implements Serializable {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer userId;
    @Column(name = "user_account")
    private String userAccount;
    @Column(name = "user_password")
    private String userPassword;
    @Column(name = "user_tel")
    private String userTel;
    @Column(name = "user_limit")
    private String userLimit;
    @Column(name = "user_creditrating")
    private String userCreditrating;

    public CcmsUser() {
    }

    public CcmsUser(String userTel) {
        this.userTel = userTel;
    }

    public CcmsUser(String userAccount, String userPassword, String userTel) {
        this.userAccount = userAccount;
        this.userPassword = userPassword;
        this.userTel = userTel;
    }

    public CcmsUser(String userAccount, String userPassword) {
        this.userAccount = userAccount;
        this.userPassword = userPassword;
    }


    public CcmsUser(Integer userId, String userAccount, String userPassword, String userTel, String userLimit, String userCreditrating) {
        this.userId = userId;
        this.userAccount = userAccount;
        this.userPassword = userPassword;
        this.userTel = userTel;
        this.userLimit = userLimit;
        this.userCreditrating = userCreditrating;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    public String getUserLimit() {
        return userLimit;
    }

    public void setUserLimit(String userLimit) {
        this.userLimit = userLimit;
    }

    public String getUserCreditrating() {
        return userCreditrating;
    }

    public void setUserCreditrating(String userCreditrating) {
        this.userCreditrating = userCreditrating;
    }

    @Override
    public String toString() {
        return "CcmsUser{" +
                "userId=" + userId +
                ", userAccount='" + userAccount + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userTel='" + userTel + '\'' +
                ", userLimit='" + userLimit + '\'' +
                ", userCreditrating='" + userCreditrating + '\'' +
                '}';
    }
}
