package entity;

import javax.persistence.Id;
import javax.persistence.Table;

@Table
public class CcmsUserInfo {
    @Id
    private Integer userId;
    private String userAccount;
    private String userPassword;
    private String userTel;
    //用户总额度
    private Double userLimit;
    //用户信用度
    private Integer userCreditrating;
    //用户账单生成日
    private String billDate;
    //订单寄送方式
    private String billType;
    //是否自动还款
    private String autoRepayment;
    //最后还款日
    private String repayment;
    //用户可用额度
    private Double availableCredit;

    public CcmsUserInfo() {
    }

    public CcmsUserInfo(Integer userId, String billType) {
        this.userId = userId;
        this.billType = billType;
    }

    public CcmsUserInfo(Integer userId, String userAccount, String userPassword, String userTel, Double userLimit, Integer userCreditrating, String billDate, String billType, String autoRepayment, String repayment, Double availableCredit) {
        this.userId = userId;
        this.userAccount = userAccount;
        this.userPassword = userPassword;
        this.userTel = userTel;
        this.userLimit = userLimit;
        this.userCreditrating = userCreditrating;
        this.billDate = billDate;
        this.billType = billType;
        this.autoRepayment = autoRepayment;
        this.repayment = repayment;
        this.availableCredit = availableCredit;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    public Double getUserLimit() {
        return userLimit;
    }

    public void setUserLimit(Double userLimit) {
        this.userLimit = userLimit;
    }

    public Integer getUserCreditrating() {
        return userCreditrating;
    }

    public void setUserCreditrating(Integer userCreditrating) {
        this.userCreditrating = userCreditrating;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getAutoRepayment() {
        return autoRepayment;
    }

    public void setAutoRepayment(String autoRepayment) {
        this.autoRepayment = autoRepayment;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public Double getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(Double availableCredit) {
        this.availableCredit = availableCredit;
    }

    @Override
    public String toString() {
        return "CcmsUserInfo{" +
                "userId=" + userId +
                ", userAccount='" + userAccount + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userTel='" + userTel + '\'' +
                ", userLimit=" + userLimit +
                ", userCreditrating=" + userCreditrating +
                ", billDate='" + billDate + '\'' +
                ", billType='" + billType + '\'' +
                ", autoRepayment='" + autoRepayment + '\'' +
                ", repayment='" + repayment + '\'' +
                ", availableCredit=" + availableCredit +
                '}';
    }
}
