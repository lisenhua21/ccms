package entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
@Table(name = "sys_permission")
public class Permission implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 权限ID
     */
    @Id
    private Integer id;

    /**
     * 父级ID
     */
    private Integer pid;

    /**
     * 权限类型，menu或permission
     */
    private String type;

    /**
     * 权限名称
     */
    private String title;

    /**
     * 权限编码，只有type= permission才有
     */
    private String percode;

    /**
     * 图标，只有type=menu才有
     */
    private String icon;

    /**
     * 权限路径
     */
    private String href;

    /**
     * 菜单类型是否可以展开
     */
    private Integer open;

    /**
     * 序号
     */
    private Integer ordernum;

    /**
     * 状态，0为不可用，1为可用
     */
    private Integer available;

    public Permission() {
    }

    public Permission(Integer id, Integer pid, String type, String title, String percode,
                      String icon, String href, Integer open, Integer ordernum, Integer available) {
        this.id = id;
        this.pid = pid;
        this.type = type;
        this.title = title;
        this.percode = percode;
        this.icon = icon;
        this.href = href;
        this.open = open;
        this.ordernum = ordernum;
        this.available = available;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPercode() {
        return percode;
    }

    public void setPercode(String percode) {
        this.percode = percode;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Integer getOpen() {
        return open;
    }

    public void setOpen(Integer open) {
        this.open = open;
    }

    public Integer getOrdernum() {
        return ordernum;
    }

    public void setOrdernum(Integer ordernum) {
        this.ordernum = ordernum;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "id=" + id +
                ", pid=" + pid +
                ", type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", percode='" + percode + '\'' +
                ", icon='" + icon + '\'' +
                ", href='" + href + '\'' +
                ", open=" + open +
                ", ordernum=" + ordernum +
                ", available=" + available +
                '}';
    }
}
