package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigInteger;

/**
 * <p>
 * 联系人信息
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_contacts")
public class TContacts {
    /**
     * 联系人ID
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer contactId;

    private Integer userId;

    private String contactName;

    private String contactPhone;

    private String contactRelation;

    private String autopayCount;

    private String methodid;
    @Transient
    private String mname;

    private String knowsbook;

    private String email;


    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getAutopayCount() {
        return autopayCount;
    }

    public void setAutopayCount(String autopayCount) {
        this.autopayCount = autopayCount;
    }

    public TContacts(String contactName, String contactPhone, String contactRelation, String autopayCount,
                     String methodid, String email) {
        this.contactName = contactName;
        this.contactPhone = contactPhone;
        this.contactRelation = contactRelation;
        this.autopayCount = autopayCount;
        this.methodid = methodid;
        this.email = email;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }



    public String getContactRelation() {
        return contactRelation;
    }

    public void setContactRelation(String contactRelation) {
        this.contactRelation = contactRelation;
    }



    public String getMethodid() {
        return methodid;
    }

    public void setMethodid(String methodid) {
        this.methodid = methodid;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getKnowsbook() {
        return knowsbook;
    }

    public void setKnowsbook(String knowsbook) {
        this.knowsbook = knowsbook;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TContacts() {
    }

    public TContacts(String contactName, String contactPhone, String contactRelation, String autopayCount,
                     String methodid, String knowsbook, String email) {
        this.contactName = contactName;
        this.contactPhone = contactPhone;
        this.contactRelation = contactRelation;
        this.autopayCount = autopayCount;
        this.methodid = methodid;
        this.knowsbook = knowsbook;
        this.email = email;
    }

    public TContacts(Integer contactId, Integer userId, String contactName, String contactPhone, String
            contactRelation, String autopayCount, String methodid, String knowsbook, String email) {
        this.contactId = contactId;
        this.userId = userId;
        this.contactName = contactName;
        this.contactPhone = contactPhone;
        this.contactRelation = contactRelation;
        this.autopayCount = autopayCount;
        this.methodid = methodid;
        this.knowsbook = knowsbook;
        this.email = email;
    }

    @Override
    public String toString() {
        return "TContacts{" +
                "contactId=" + contactId +
                ", userId=" + userId +
                ", contactName='" + contactName + '\'' +
                ", contactPhone='" + contactPhone + '\'' +
                ", contactRelation='" + contactRelation + '\'' +
                ", autopayCount='" + autopayCount + '\'' +
                ", methodid='" + methodid + '\'' +
                ", mname='" + mname + '\'' +
                ", knowsbook='" + knowsbook + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}