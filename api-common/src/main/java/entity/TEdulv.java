package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * 受教育程度
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_eduLv")
public class TEdulv {
    /**
     * 受教育程度ID
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer educationid;

    private String level;

    public TEdulv(Integer educationid, String level) {
        this.educationid = educationid;
        this.level = level;
    }

    public TEdulv() {
        super();
    }

    public Integer getEducationid() {
        return educationid;
    }

    public void setEducationid(Integer educationid) {
        this.educationid = educationid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level == null ? null : level.trim();
    }
}