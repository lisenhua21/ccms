package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * <p>
 * 基本信息
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_essential")
public class TEssential {
    /**
     * 基本信息
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer essentialid;

    private String realname;

    private String identityid;

    private String phonenumber;

    private Integer nationname;
    @Transient
    private String nname;

    private Integer maritalid;
    @Transient
    private String mname;

    private Integer educationid;
    @Transient
    private String ename;

    private Integer questionid;
    @Transient
    private String qname;

    private String answer;

    private Integer cityid;
    @Transient
    private String cname;

    private String house;

    private Integer postnum;

    private Integer userId;

    public TEssential(Integer essentialid, String realname, String identityid, String phonenumber, Integer
            nationname, Integer maritalid, Integer educationid, Integer questionid, String answer, Integer cityid,
                      String house, Integer postnum, Integer userId) {
        this.essentialid = essentialid;
        this.realname = realname;
        this.identityid = identityid;
        this.phonenumber = phonenumber;
        this.nationname = nationname;
        this.maritalid = maritalid;
        this.educationid = educationid;
        this.questionid = questionid;
        this.answer = answer;
        this.cityid = cityid;
        this.house = house;
        this.postnum = postnum;
        this.userId = userId;
    }

    public TEssential() {
        super();
    }

    public Integer getEssentialid() {
        return essentialid;
    }

    public void setEssentialid(Integer essentialid) {
        this.essentialid = essentialid;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname == null ? null : realname.trim();
    }

    public String getIdentityid() {
        return identityid;
    }

    public void setIdentityid(String identityid) {
        this.identityid = identityid == null ? null : identityid.trim();
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber == null ? null : phonenumber.trim();
    }

    public Integer getNationname() {
        return nationname;
    }

    public void setNationname(Integer nationname) {
        this.nationname = nationname;
    }

    public Integer getMaritalid() {
        return maritalid;
    }

    public void setMaritalid(Integer maritalid) {
        this.maritalid = maritalid;
    }

    public Integer getEducationid() {
        return educationid;
    }

    public void setEducationid(Integer educationid) {
        this.educationid = educationid;
    }

    public Integer getQuestionid() {
        return questionid;
    }

    public void setQuestionid(Integer questionid) {
        this.questionid = questionid;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }

    public Integer getCityid() {
        return cityid;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house == null ? null : house.trim();
    }

    public Integer getPostnum() {
        return postnum;
    }

    public void setPostnum(Integer postnum) {
        this.postnum = postnum;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNname() {
        return nname;
    }

    public void setNname(String nname) {
        this.nname = nname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getQname() {
        return qname;
    }

    public void setQname(String qname) {
        this.qname = qname;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
}