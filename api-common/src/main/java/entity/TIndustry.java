package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 *  行业
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_industry")
public class TIndustry {
    /**
     * 行业ID
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer industryid;

    private String industryname;

    public TIndustry(Integer industryid, String industryname) {
        this.industryid = industryid;
        this.industryname = industryname;
    }

    public TIndustry() {
        super();
    }

    public Integer getIndustryid() {
        return industryid;
    }

    public void setIndustryid(Integer industryid) {
        this.industryid = industryid;
    }

    public String getIndustryname() {
        return industryname;
    }

    public void setIndustryname(String industryname) {
        this.industryname = industryname == null ? null : industryname.trim();
    }
}