package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * 婚姻状况
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_marital")
public class TMarital {
    /**
     * 婚姻状况ID
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer mid;

    private String status;

    public TMarital(Integer mid, String status) {
        this.mid = mid;
        this.status = status;
    }

    public TMarital() {
        super();
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}