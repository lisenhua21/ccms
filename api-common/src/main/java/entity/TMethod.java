package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * 领卡方式
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_method")
public class TMethod {
    /**
     * 领卡方式ID
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer methodid;

    private String method;

    public TMethod(Integer methodid, String method) {
        this.methodid = methodid;
        this.method = method;
    }

    public TMethod() {
        super();
    }

    public Integer getMethodid() {
        return methodid;
    }

    public void setMethodid(Integer methodid) {
        this.methodid = methodid;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method == null ? null : method.trim();
    }
}