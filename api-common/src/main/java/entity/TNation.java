package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * 民族
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_nation")
public class TNation {
    /**
     * 民族ID
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer nid;

    private String nationname;

    public TNation(Integer nid, String nationname) {
        this.nid = nid;
        this.nationname = nationname;
    }

    public TNation() {
        super();
    }

    public Integer getNid() {
        return nid;
    }

    public void setNid(Integer nid) {
        this.nid = nid;
    }

    public String getNationname() {
        return nationname;
    }

    public void setNationname(String nationname) {
        this.nationname = nationname == null ? null : nationname.trim();
    }
}