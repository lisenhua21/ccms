package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * 职务
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_position")
public class TPosition {
    /**
     * 职位id
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer positionid;

    private String positionname;

    public TPosition(Integer positionid, String positionname) {
        this.positionid = positionid;
        this.positionname = positionname;
    }

    public Integer getPositionid() {
        return positionid;
    }

    public void setPositionid(Integer positionid) {
        this.positionid = positionid;
    }

    public String getPositionname() {
        return positionname;
    }

    public void setPositionname(String positionname) {
        this.positionname = positionname;
    }
}