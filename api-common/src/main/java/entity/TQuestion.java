package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 *  密保问题
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_question")
public class TQuestion {
    /**
     * 密保问题ID
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer questionid;

    private String question;

    public TQuestion(Integer questionid, String question) {
        this.questionid = questionid;
        this.question = question;
    }

    public TQuestion() {
        super();
    }

    public Integer getQuestionid() {
        return questionid;
    }

    public void setQuestionid(Integer questionid) {
        this.questionid = questionid;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question == null ? null : question.trim();
    }
}