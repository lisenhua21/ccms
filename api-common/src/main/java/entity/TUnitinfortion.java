package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * <p>
 * 单位信息
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_unitinfortion")
public class TUnitinfortion {
    /**
     * 单位信息ID
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer uid;

    private String unitname;

    private Integer natureid;
    @Transient
    private String nname;

    private Integer industryid;
    @Transient
    private String iname;

    private Integer cityid;
    @Transient
    private String cname;

    private Integer positonid;
    @Transient
    private String pname;

    private Integer annualincome;

    private Integer userId;

    private Integer unitPhone;

    private Integer upostnumber;


    public TUnitinfortion(Integer uid, String unitname, Integer natureid, Integer industryid, Integer cityId, Integer
            positonid, Integer annualincome, Integer userId, Integer unitPhone, Integer upostnumber) {
        this.uid = uid;
        this.unitname = unitname;
        this.natureid = natureid;
        this.industryid = industryid;
        this.cityid = cityId;
        this.positonid = positonid;
        this.annualincome = annualincome;
        this.userId = userId;
        this.unitPhone = unitPhone;
        this.upostnumber = upostnumber;
    }

    public TUnitinfortion(String unitname, Integer natureid, Integer industryid, Integer cityId, Integer positonid,
                          Integer annualincome, Integer unitPhone, Integer upostnumber) {
        this.unitname = unitname;
        this.natureid = natureid;
        this.industryid = industryid;
        this.cityid = cityId;
        this.positonid = positonid;
        this.annualincome = annualincome;
        this.unitPhone = unitPhone;
        this.upostnumber = upostnumber;
    }

    public Integer getCityid() {
        return cityid;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }


    public Integer getUpostnumber() {
        return upostnumber;
    }

    public void setUpostnumber(Integer upostnumber) {
        this.upostnumber = upostnumber;
    }

    public TUnitinfortion(Integer uid, String unitname, Integer natureid, Integer industryid, Integer cityId, Integer positonid, Integer annualincome, Integer userId, Integer unitPhone) {
        this.uid = uid;
        this.unitname = unitname;
        this.natureid = natureid;
        this.industryid = industryid;
        this.cityid = cityId;
        this.positonid = positonid;
        this.annualincome = annualincome;
        this.userId = userId;
        this.unitPhone = unitPhone;
    }

    public TUnitinfortion() {
        super();
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname == null ? null : unitname.trim();
    }

    public Integer getNatureid() {
        return natureid;
    }

    public void setNatureid(Integer natureid) {
        this.natureid = natureid;
    }

    public Integer getIndustryid() {
        return industryid;
    }

    public void setIndustryid(Integer industryid) {
        this.industryid = industryid;
    }


    public Integer getPositonid() {
        return positonid;
    }

    public void setPositonid(Integer positonid) {
        this.positonid = positonid;
    }

    public Integer getAnnualincome() {
        return annualincome;
    }

    public void setAnnualincome(Integer annualincome) {
        this.annualincome = annualincome;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUnitPhone() {
        return unitPhone;
    }

    public void setUnitPhone(Integer unitPhone) {
        this.unitPhone = unitPhone;
    }

    public String getNname() {
        return nname;
    }

    public void setNname(String nname) {
        this.nname = nname;
    }

    public String getIname() {
        return iname;
    }

    public void setIname(String iname) {
        this.iname = iname;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    @Override
    public String toString() {
        return "TUnitinfortion{" +
                "uid=" + uid +
                ", unitname='" + unitname + '\'' +
                ", natureid=" + natureid +
                ", nname='" + nname + '\'' +
                ", industryid=" + industryid +
                ", iname='" + iname + '\'' +
                ", cityid=" + cityid +
                ", cname='" + cname + '\'' +
                ", positonid=" + positonid +
                ", pname='" + pname + '\'' +
                ", annualincome=" + annualincome +
                ", userId=" + userId +
                ", unitPhone=" + unitPhone +
                ", upostnumber=" + upostnumber +
                '}';
    }
}