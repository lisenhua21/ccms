package entity;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * 单位性质
 * </p>
 *
 * @author liso
 * @since 2020-09-23
 */
@Table(name = "t_unitnature")
public class TUnitnature {
    /**
     * 单位性质ID
     */
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer natureid;

    private String nature;

    public TUnitnature(Integer natureid, String nature) {
        this.natureid = natureid;
        this.nature = nature;
    }

    public TUnitnature() {
        super();
    }

    public Integer getNatureid() {
        return natureid;
    }

    public void setNatureid(Integer natureid) {
        this.natureid = natureid;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature == null ? null : nature.trim();
    }
}