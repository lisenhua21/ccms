package entity;

import tk.mybatis.mapper.annotation.KeySql;
import utils.BigDecimalUtls;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 主要用来接收查询的数据,不是提交审核的类
 */
@Table(name = "ccms_installment")
public class TemporaryInstallment {
    @Id
    private String installmentId;           //这是分期付款申请表
    private int billId;                     //这是账单id
    private String creditcardNumber;        //这是信用卡号
    private double ccmsUserLimit;           //这是账户的总额度
    private int ccmsUserCreditrating;       //这是账户的信用度
    private String ccmsBillCreate;          //分期的账单时间
    private double ccmsBillAmount;          //本月账单已消费金额
    private double maxInstallment=0;        //本月账单最大可分期金额
    private double installmentMoney;        //想分期多少,不得超过总消费的九成
    private int installmentDateId;          //分期次数,分几期.
    private int interest;                   //选择分期的利息,
    private double paySurplus;              //剩余应还多少,本月还

    @Override
    public String toString() {
        return "TemporaryInstallment{" +
                "installmentId='" + installmentId + '\'' +
                ", billId=" + billId +
                ", creditcardNumber='" + creditcardNumber + '\'' +
                ", ccmsUserLimit=" + ccmsUserLimit +
                ", ccmsUserCreditrating=" + ccmsUserCreditrating +
                ", ccmsBillCreate='" + ccmsBillCreate + '\'' +
                ", ccmsBillAmount=" + ccmsBillAmount +
                ", maxInstallment=" + maxInstallment +
                ", installmentMoney=" + installmentMoney +
                ", installmentDateId=" + installmentDateId +
                ", interest=" + interest +
                ", paySurplus=" + paySurplus +
                '}';
    }

    public String getInstallmentId() {
        return installmentId;
    }

    public void setInstallmentId(String installmentId) {
        this.installmentId = installmentId;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public String getCreditcardNumber() {
        return creditcardNumber;
    }

    public void setCreditcardNumber(String creditcardNumber) {
        this.creditcardNumber = creditcardNumber;
    }

    public double getCcmsUserLimit() {
        return ccmsUserLimit;
    }

    public void setCcmsUserLimit(double ccmsUserLimit) {
        this.ccmsUserLimit = ccmsUserLimit;
    }

    public int getCcmsUserCreditrating() {
        return ccmsUserCreditrating;
    }

    public void setCcmsUserCreditrating(int ccmsUserCreditrating) {
        this.ccmsUserCreditrating = ccmsUserCreditrating;
    }

    public String getCcmsBillCreate() {
        return ccmsBillCreate;
    }

    public void setCcmsBillCreate(String ccmsBillCreate) {
        this.ccmsBillCreate = ccmsBillCreate;
    }

    public double getCcmsBillAmount() {
        return ccmsBillAmount;
    }

    public void setCcmsBillAmount(double ccmsBillAmount) {
        this.ccmsBillAmount = ccmsBillAmount;
    }

    public void setMaxInstallment(double maxInstallment) {
        this.maxInstallment = maxInstallment;
    }

    public double getMaxInstallment() {
        double d =0;
        if (this.ccmsUserCreditrating<450){
            d=0.6;
        }else {
            d=0.9;
        }
        //这是账户最大可分期金额
        double s = BigDecimalUtls.mul(this.ccmsBillAmount,d);
        this.maxInstallment=s;
        return s;
    }

    public double getInstallmentMoney() {
        return installmentMoney;
    }

    public void setInstallmentMoney(double installmentMoney) {
        this.installmentMoney = installmentMoney;
    }

    public int getInstallmentDateId() {
        return installmentDateId;
    }

    public void setInstallmentDateId(int installmentDateId) {
        this.installmentDateId = installmentDateId;
    }

    public int getInterest() {
        return interest;
    }

    public void setInterest(int interest) {
        this.interest = interest;
    }

    public double getPaySurplus() {
        return paySurplus;
    }

    public void setPaySurplus(double paySurplus) {
        this.paySurplus = paySurplus;
    }

    public TemporaryInstallment() {
    }

    public TemporaryInstallment(String installmentId, int billId, String creditcardNumber, double ccmsUserLimit, int ccmsUserCreditrating, String ccmsBillCreate, double ccmsBillAmount, double maxInstallment, double installmentMoney, int installmentDateId, int interest, double paySurplus) {
        this.installmentId = installmentId;
        this.billId = billId;
        this.creditcardNumber = creditcardNumber;
        this.ccmsUserLimit = ccmsUserLimit;
        this.ccmsUserCreditrating = ccmsUserCreditrating;
        this.ccmsBillCreate = ccmsBillCreate;
        this.ccmsBillAmount = ccmsBillAmount;
        this.maxInstallment = maxInstallment;
        this.installmentMoney = installmentMoney;
        this.installmentDateId = installmentDateId;
        this.interest = interest;
        this.paySurplus = paySurplus;
    }
}