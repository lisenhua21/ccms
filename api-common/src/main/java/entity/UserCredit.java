package entity;

public class UserCredit {
    private String certificate_num;  //身份证号
    private long cardnum;       //卡号
    private String userName;    //用户名
    private String active;     //是否激活
    private String status;     //状态
    private String phonenumber;  //联系电话

    public UserCredit() {
    }

    public UserCredit(String certificate_num, long cardnum, String userName, String active, String status, String phonenumber) {
        this.certificate_num = certificate_num;
        this.cardnum = cardnum;
        this.userName = userName;
        this.active = active;
        this.status = status;
        this.phonenumber = phonenumber;
    }

    public UserCredit(String certificate_num, long cardnum, String userName, String active) {
        this.certificate_num = certificate_num;
        this.cardnum = cardnum;
        this.userName = userName;
        this.active = active;
    }

    public UserCredit(String certificate_num, long cardnum, String userName, String active, String status) {
        this.certificate_num = certificate_num;
        this.cardnum = cardnum;
        this.userName = userName;
        this.active = active;
        this.status = status;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getCertificate_num() {
        return certificate_num;
    }

    public void setCertificate_num(String certificate_num) {
        this.certificate_num = certificate_num;
    }

    public long getCardnum() {
        return cardnum;
    }

    public void setCardnum(long cardnum) {
        this.cardnum = cardnum;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UserCredit{" +
                "Certificate_num='" + certificate_num + '\'' +
                ", cardnum=" + cardnum +
                ", userName='" + userName + '\'' +
                ", active='" + active + '\'' +
                '}';
    }
}
