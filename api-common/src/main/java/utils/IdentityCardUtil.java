package utils;

import com.alibaba.fastjson.JSON;
import com.aliyun.com.viapi.FileUtils;
import com.aliyun.ocr20191230.Client;
import com.aliyun.ocr20191230.models.RecognizeIdentityCardRequest;
import com.aliyun.ocr20191230.models.RecognizeIdentityCardResponse;
import com.aliyun.tearpc.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.aliyuncs.exceptions.ClientException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;

/**
 * 身份证识别工具类
 */
public class IdentityCardUtil {
    //阿里云接口相关参数
    private static String accessKeyId = "LTAI4GAcsAScaod6ZjVFo7hY";
    private static String accessKeySecret = "JKlvkdvc3dXmaYPcT4cn29enNBA0VQ";
    private static Config config = new Config();
    private static RuntimeOptions runtimeOptions = new RuntimeOptions();
    static {
        config.accessKeyId = accessKeyId;
        config.accessKeySecret = accessKeySecret;
        config.regionId = "cn-shanghai";
        config.endpointType = "internal";
        config.endpoint="ocr.cn-shanghai.aliyuncs.com";
    }

    /**
     * 输入流 身份识别
     * @param is 输入流
     * @param side 身份证的正反----"face":人像面;"back":国徽面
     * @return JSON字符串
     *          反面:{
     *              "backResult": {
     *              "endDate": "20xx0808",
     *              "issue": "岳阳XXXXXXXX安分局",
     *              "startDate": "20xx0808"
     *              }
     *         }
     *         正面:{
     *         "frontResult": {
     *             "IDNumber": "4306XXXXXXXXXXXX79",
     *             "address": "湖南XXXXXXXXXXXXX组",
     *             "birthDate": "19xxxx28",
     *             "gender": "男",
     *             "iDNumber": "4306XXXXXXXXXXXX79",
     *             "name": "王X",
     *             "nationality": "汉"
     *             }
     *         }
     */
    public static String searchWithInputStream(InputStream is,String side) throws Exception {
        RecognizeIdentityCardRequest request = new RecognizeIdentityCardRequest();
        RecognizeIdentityCardResponse response = null;
        Client client = null;
        client = new Client(config);
        String url = MyFileUtils.getInstance(accessKeyId,accessKeySecret).upload(is);
        request.imageURL = url;
        request.side = side;
        response = client.recognizeIdentityCard(request,runtimeOptions);
        response.getData();
        return JSON.toJSONString(response.getData());
    }

    /**
     *
     * @param filePath 本地文件路径
     * @param side 身份证的正反----"face":人像面;"back":国徽面
     * @return
     * @throws Exception
     */
    public static String searchWithFile(String filePath,String side) throws Exception {
        RecognizeIdentityCardRequest request = new RecognizeIdentityCardRequest();
        RecognizeIdentityCardResponse response = null;
        Client client = null;
        client = new Client(config);
        String url = FileUtils.getInstance(accessKeyId,accessKeySecret).upload(filePath);
        request.imageURL = url;
        request.side = side;
        response = client.recognizeIdentityCard(request,runtimeOptions);
        response.getData();
        return JSON.toJSONString(response.getData());
    }

    /**
     * multipartFile和inputStream 转换方法
     * @param multipartFile
     * @return InputStream
     * @throws IOException
     */
    public static InputStream M2Iconvert(MultipartFile multipartFile) throws IOException {
        CommonsMultipartFile cfile = (CommonsMultipartFile) multipartFile;
        DiskFileItem dfile = (DiskFileItem) cfile.getFileItem();
        return dfile.getInputStream();
    }
}
