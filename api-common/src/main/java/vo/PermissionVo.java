package vo;

import entity.Permission;

public class PermissionVo extends Permission {

    private static final long serialVersionUID = 1L;
    private Integer page;
    private Integer limit;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "PermissionVo{" +
                "page=" + page +
                ", limit=" + limit +
                '}';
    }
}
