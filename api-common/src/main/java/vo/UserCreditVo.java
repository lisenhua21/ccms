package vo;

import entity.CcmsCredit;
import entity.UserCredit;

public class UserCreditVo extends UserCredit {
    private Integer page;
    private Integer limit;

    public UserCreditVo() {
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "CreditVo{" +
                "page=" + page +
                ", limit=" + limit +
                '}';
    }
}
