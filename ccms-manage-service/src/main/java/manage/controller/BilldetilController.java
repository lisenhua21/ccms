package manage.controller;

import commons.PageResult;
import entity.CcmsBankcard;
import entity.CcmsBill;
import entity.CcmsCredit;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import manage.service.CreditService;
import manage.service.IBankcardService;
import manage.service.IBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


@Controller
@RequestMapping("/billdetil")
public class BilldetilController {
    @Autowired
    IBillService iBillService;
    @Autowired
    IBankcardService iBankcardService;
    @Autowired
    CreditService creditService;
    @ApiIgnore
    @GetMapping("go")
    public String go(){
        return "billdetil";
    }

    @ApiOperation(value = "应还款账单分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageIndex",value = "第几页",required = true,dataType = "int"),
            @ApiImplicitParam(name = "pageSize",value = "每页条数",required = true,dataType = "int"),
            @ApiImplicitParam(name = "cardnum",value = "信用卡号",required = true,dataType = "String"),
            @ApiImplicitParam(name = "cardnum",value = "月份格式如:2020-10",required = true,dataType = "String")
    })
    @PostMapping("/list")
    @ResponseBody
    public PageResult getbill(int pageIndex, int pageSize, String cardnum, String month){
        if (cardnum==null){
            cardnum="";
        }
        PageResult pageResult = iBillService.getBillByCardnum(pageIndex, pageSize, cardnum,month);
        return pageResult;
    }
    @ApiOperation(value = "跳到还款页面")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "billid",value = "账单id",required = true,dataType = "int")
    })
    @RequestMapping("repayment")
    public String getbill(int billid, Model model){
        CcmsBill bill = iBillService.getbillByPrimaryKey(billid);
        List<CcmsBankcard> allBankcard = iBankcardService.getAllBankcard();
        Integer creditId = bill.getCreditId();
        CcmsCredit credit =  creditService.getCreditById(creditId);
        model.addAttribute("credit",credit);
        model.addAttribute("allBankcard",allBankcard);
        model.addAttribute("bill",bill);
        return "repayment";
    }
}
