package manage.controller;

import commons.LayuiResult;
import commons.ResultBean;
import entity.CcmsCredit;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import manage.service.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import vo.UserCreditVo;

import java.util.List;

@Controller
@RequestMapping("/credit")
public class CreditController {

    @Autowired
    CreditService creditService;

    /**
     * 修改密码查询所有的卡
     * @param userCreditVo
     * @return
     */
    @ApiOperation(value = "修改密码查询所有的卡")
    @ApiImplicitParam(name = "userCreditVo",value = "带分页的用户信用卡实体类对象",required = true,dataType = "UserCreditVo")
    @GetMapping(value = "/toupdpwd")
    @ResponseBody
//    @RequestMapping("/toupdpwd")
    public LayuiResult toupdpwd(UserCreditVo userCreditVo){
        LayuiResult layuiResult=creditService.getCredit5(userCreditVo);
        return layuiResult;
    }

    /**
     * 挂失查询所有申请挂失的卡
     * @param userCreditVo
     * @return
     */
    @ApiOperation(value = "挂失查询所有申请挂失的卡")
    @ApiImplicitParam(name = "userCreditVo",value = "带分页的用户信用卡实体类对象",required = true,dataType = "UserCreditVo")
    @GetMapping(value = "/toloss")
//    @RequestMapping("/toloss")
    @ResponseBody
    public LayuiResult toloss(UserCreditVo userCreditVo){
        LayuiResult layuiResult=creditService.getCredit1(userCreditVo);
        return layuiResult;
    }

    /**
     * 冻结查询所有申请冻结的卡
     * @param userCreditVo
     * @return
     */
    @ApiOperation(value = "查询所有申请冻结的卡")
    @ApiImplicitParam(name = "userCreditVo",value = "带分页的用户信用卡实体类对象",required = true,dataType = "UserCreditVo")
    @GetMapping(value = "/tofreeze")
//    @RequestMapping("/tofreeze")
    @ResponseBody
    public LayuiResult tofreeze(UserCreditVo userCreditVo){
        LayuiResult layuiResult=creditService.getCredit2(userCreditVo);
        return layuiResult;
    }

    /**
     * 注销查询所有申请注销的卡
     * @param userCreditVo
     * @return
     */
    @ApiOperation(value = "查询所有申请注销的卡")
    @ApiImplicitParam(name = "userCreditVo",value = "带分页的用户信用卡实体类对象",required = true,dataType = "UserCreditVo")
    @GetMapping(value = "/tologout")
//    @RequestMapping("/tologout")
    @ResponseBody
    public LayuiResult tologout(UserCreditVo userCreditVo){
        LayuiResult layuiResult=creditService.getCredit3(userCreditVo);
        return layuiResult;
    }


    /**
     * 激活查询需要激活的卡
     * @param userCreditVo
     * @return
     */
    @ApiOperation(value = "查询待激活的卡")
    @ApiImplicitParam(name = "userCreditVo",value = "带分页的用户信用卡实体类对象",required = true,dataType = "UserCreditVo")
    @GetMapping(value = "/toactive")
//    @RequestMapping("/toactive")
    @ResponseBody
    public LayuiResult toactive(UserCreditVo userCreditVo){
        LayuiResult layuiResult=creditService.getCredit4(userCreditVo);
        return layuiResult;
    }

    /**
     * 修改使用状态
     * @param cardnum
     * @param type
     * @return
     */
    @ApiOperation(value = "修改使用状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cardnum",value = "信用卡号",required = true,dataType = "long"),
            @ApiImplicitParam(name = "type",value = "使用状态",required = true,dataType = "int"),
    })
    @PostMapping(value = "/upstatus")
//    @RequestMapping("/upstatus")
    @ResponseBody
    public LayuiResult upstatus(long cardnum,int type){
        CcmsCredit ccmsCredit=creditService.getCredit(cardnum);
        //type 1正常使用 2 代表挂失中,3代表已挂失，4代表申请冻结，5代表已冻结，6代表申请注销，7已注销
        LayuiResult rb=creditService.updateStatus(ccmsCredit,type);
        //缺发送短信
        return rb;
    }

    /**
     * 修改密码
     * @param opwd
     * @param npwd
     * @param cardnum
     * @return
     */
    @ApiOperation(value = "修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "opwd",value = "旧密码",required = true,dataType = "String"),
            @ApiImplicitParam(name = "npwd",value = "新密码",required = true,dataType = "String"),
            @ApiImplicitParam(name = "cardnum",value = "信用卡号",required = true,dataType = "long"),
    })
    @PostMapping(value = "/updpwd")
//    @RequestMapping("/updpwd")
    @ResponseBody
    public LayuiResult updpwd(String opwd,String npwd,long cardnum){
        CcmsCredit c=creditService.getCredit(cardnum);
        if(c.getCreditPwd().equals(opwd)){
            c.setCreditPwd(npwd);
            int s=creditService.updataActive(c);
            if(s>0){
                return new LayuiResult("修改成功");
            }else {
                return new LayuiResult("修改失败");
            }
        }else {
            return new LayuiResult("密码输入错误");
        }

    }


    /**
     * 修改激活状态
     * @param cardnum
     * @param type
     * @return
     */
    @ApiOperation(value = "修改激活状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cardnum",value = "信用卡号",required = true,dataType = "long"),
            @ApiImplicitParam(name = "type",value = "激活状态",required = true,dataType = "int"),
    })
    @PostMapping(value = "/upactive")
//    @RequestMapping("/upactive")
    @ResponseBody
    public LayuiResult upactive(long cardnum,int type){
        CcmsCredit c=creditService.getCredit(cardnum);
        if(type==1){
            c.setActive("已激活");
        }else {
            c.setActive("未激活");
        }
        int s=creditService.updataActive(c);
        if(s>0){
            return new LayuiResult("修改成功");
        }else {
            return new LayuiResult("修改失败");
        }
    }
}
