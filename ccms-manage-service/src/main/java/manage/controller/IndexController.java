package manage.controller;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 这个controller负责页面跳转
 */
@Controller
public class IndexController {

    /**
     * 跳转到主页面
     * @return
     */
    @RequestMapping(value = {"/index","/"})
    public String index(){
        return "index";
    }

    /**
     * 跳转到菜单管理页面
     * @return
     */
    @RequestMapping("/menu/page")
    @RequiresPermissions("menu:select")
    public String menuPage(){
        return "system/menu";
    }

    /**
     * 跳转到登录页面
     * @return
     */
    @RequestMapping("/login/page")
    public String loginPage(){
        return "login";
    }


    /**
     * 跳转到图标页面
     * @return
     */
    @RequestMapping("/icon/page")
    public String iconPage(){
        return "system/icon";
    }

    /**
     * 跳转到权限页面
     * @return
     */
    @RequestMapping("/permission/page")
    @RequiresPermissions("permission:select")
    public String permissionPage(){
        return "system/permission";
    }

    /**
     * 跳转到用户页面
     * @return
     */
    @RequestMapping("/admin/page")
    @RequiresPermissions("admin:select")
    public String adminPage(){
        return "system/admin";
    }


    /**
     * 跳转到角色页面
     * @return
     */
    @RequestMapping("/role/page")
    @RequiresPermissions("role:select")
    public String rolePage(){
        return "system/role";
    }

    @RequestMapping("/system/main")
    @RequiresAuthentication
    public String mainPage(){
        return "main";
    }

    /**
     * 跳转到激活页面
     * @return
     */
    @RequestMapping("/active/page")
    public String jihuoPage(){
        return "creditManager/active";
    }

    /**
     * 跳转到卡片冻结页面
     * @return
     */
    @RequestMapping("/freeze/page")
    public String freezePage(){
        return "creditManager/freeze";
    }

    /**
     * 跳转到挂失页面
     * @return
     */
    @RequestMapping("/loss/page")
    public String lossPage(){
        return "creditManager/loss";
    }

    /**
     * 跳转到注销页面
     * @return
     */
    @RequestMapping("/logout/page")
    public String logoutPage(){
        return "creditManager/logout";
    }


    /**
     * 跳转到修改密码页面
     * @return
     */
    @RequestMapping("/uppwd/page")
    public String uppwdPage(){
        return "creditManager/updpwd";
    }

}
