package manage.controller;

import commons.LayuiResult;
import entity.Permission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import manage.service.PermissionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vo.PermissionVo;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/menu")
@Api(tags = "菜单管理相关模块")
public class MenuController {

    @Autowired
    private PermissionService permissionService;

    /**
     * 加载菜单
     * @return
     */
    @GetMapping("/loadMenuJson")
    @ApiOperation(value = "根据管理员角色的不同加载不同的菜单")
    public LayuiResult loadMenuJson(){
        LayuiResult layuiResult = permissionService.loadMenuJson();
        return layuiResult;
    }

    /**
     * 加载菜单管理页面左侧的菜单Json树
     * @return
     */
    @GetMapping("/loadMenuDTreeJson")
    @RequiresPermissions("menu:select")
    @ApiOperation(value = "加载菜单管理页面左侧的菜单Json树")
    public LayuiResult loadMenuPageLeftDTreeJson() {
        LayuiResult layuiResult = permissionService.loadLeftMenuJson();
        return layuiResult;
    }

    /**
     * 加载菜单表
     * @param permissionVo
     * @return
     */
    @GetMapping("/loadAllMenus")
    @RequiresPermissions("menu:select")
    @ApiOperation(value = "加载菜单表")
    @ApiImplicitParam(name = "permissionVo",value = "菜单列表",required = true,dataType = "PermissionVo")
    public LayuiResult loadAllMenus(PermissionVo permissionVo) {
        LayuiResult layuiResult = permissionService.page(permissionVo);
        return layuiResult;
    }
}
