package manage.controller;

import com.sun.xml.bind.v2.model.core.ID;
import entity.CcmsCity;
import entity.CcmsCredit;
import entity.TEssential;
import manage.mapper.CreditMapper;
import manage.mapper.TEssentialMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 逾期还款管理
 * </p>
 *
 * @author liso
 * @since 2020/10/9 10:28
 */
@Controller
@RequestMapping("/overdue")
public class OverdueController {

    @Autowired
    private CreditMapper creditMapper;
    @Autowired
    private TEssentialMapper essentialMapper;

    /**
     * 功能描述：返回日期
     *
     * @param date Date 日期
     * @return 返回日份
     */
    private static int getDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * <p>
     * 传入信用卡list 将它们的基本信息返回
     * </p>
     *
     * @author liso
     * @since 2020/10/9 10:50
     * * @param List ccmsCredits
     * * @return List TEssential
     */
    private List<TEssential> getEssential(List<CcmsCredit> ccmsCredits) {
        List Idlist = new ArrayList();
        for (CcmsCredit ccmsCredit : ccmsCredits) {
            int essentialId = ccmsCredit.getEssentialId();
            Idlist.add(essentialId);
        }
        Example ex = new Example(TEssential.class);
        Example.Criteria criteria1 = ex.createCriteria();
        criteria1.andIn("", Idlist);
        return essentialMapper.selectByExample(ex);
    }

    /**
     * <p>
     * 定时器 每天凌晨向需要还款且距离还款日期只剩两天的用户发送还款提醒
     * </p>
     *
     * @author liso
     * @since 2020/10/9 10:32
     * * @param null
     * * @return null
     */
    @Scheduled(cron = "0 0 1 * * ?")
    protected void task() {
        int day = getDay(new Date());
        System.out.println(day);
        Example o = new Example(CcmsCity.class);
        Example.Criteria criteria = o.createCriteria();
        criteria.andGreaterThan("debt", 0);
        criteria.andBetween("repayment", day, day + 2);
        List<CcmsCredit> ccmsCredits = creditMapper.selectByExample(o);
        getEssential(ccmsCredits);
    }

    /**
     * <p>
     *
     * </p>
     *
     * @author liso
     * @since 2020/10/9 11:28
     * * @param null
     * * @return List<CcmsCredit>
     */
    @RequestMapping("/overlist")
    public List<CcmsCredit> selectrepay() {
        int day = getDay(new Date());
        Example o = new Example(CcmsCity.class);
        Example.Criteria criteria = o.createCriteria();
        criteria.andGreaterThan("debt", 0);
        criteria.andLessThan("repayment", day);
        return creditMapper.selectByExample(o);

    }


}
