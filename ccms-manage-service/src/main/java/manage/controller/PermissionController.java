package manage.controller;

import commons.LayuiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import manage.service.PermissionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vo.PermissionVo;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/permission")
@Api(tags = "权限相关模块")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    /**
     * 权限页面左侧的Json树
     * @return
     */
    @GetMapping("/loadPermissionDTreeJson")
    @RequiresPermissions("permission:select")
    @ApiOperation(value = "加载权限页面左侧的权限树")
    public LayuiResult loadPermissionLeftDTreeJson() {
        LayuiResult layuiResult = permissionService.loadPermissionLeftDTreeJson();
        return layuiResult;
    }

    /**
     * 加载所有权限或通过点击权限页面左侧权限树加载该菜单下的权限
     * @param permissionVo
     * @return
     */
    @GetMapping("/loadPermissions")
    @RequiresPermissions("permission:select")
    @ApiOperation(value = "加载所有权限或通过点击权限页面左侧权限树加载该菜单下的权限")
    @ApiImplicitParam(name = "permissionVo",value = "权限列表",required = true,dataType = "PermissionVo")
    public LayuiResult loadPermissions(PermissionVo permissionVo) {
        LayuiResult layuiResult = permissionService.loadPermissions(permissionVo);
        return layuiResult;
    }
}
