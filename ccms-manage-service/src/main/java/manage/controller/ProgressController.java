package manage.controller;


import commons.PageResult;
import entity.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import manage.service.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>
 *  信用卡审核模块
 * </p>
 *
 * @author liso
 * @since 2020/10/14 17:36
 */
@Api(tags = "信用卡审核模块")
@RestController
@RequestMapping("/progress")
public class ProgressController {
    //cardprogress表
    @Autowired
    ICardProgressService iCardProgressService;
    //t_essential表
    @Autowired
    ITEssentialService itEssentialService;
    //t_unitinfortion表
    @Autowired
    ITUnitinfortionService itUnitinfortionService;
    //t_contacts表
    @Autowired
    ITContactsService itContactsService;

//    public static String FORMAT_LONG_CN = "yyyy-MM-dd  HH:mm:ss";
//
//    /**
//     * 获取当前时间
//     */
//    public static String getTimeString(Date date) {
//        SimpleDateFormat df = new SimpleDateFormat(FORMAT_LONG_CN);
//        return df.format(date);
//    }


    /**
     * <p>
     * 审核不通过 备注原因
     * </p>
     *
     * @author liso
     * @since 2020/10/13 9:27
     * * @param cardProgress
     * * @return java.lang.String
     */
    @ApiOperation(value = "审核不通过", notes = "备注失败原因")
    @ApiImplicitParam(name = "cardProgress", value = "申请卡进程对象", required = true, paramType = "body", dataTypeClass =
            CardProgress.class)
    @PostMapping("/fail")
    @ResponseBody
    public String fail(@RequestBody CardProgress cardProgress) {
        Date date = new Date();
        cardProgress.setCpDealTime(date);
        //1.获取subject和Seesion
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        Admin admin = (Admin) session.getAttribute("admin");
        cardProgress.setCpDealAdmin(admin.getId());
        cardProgress.setCpStatus("失败");
        if (iCardProgressService.examineUpdate(cardProgress) == 1) {
            return "审核完成";
        }
        return "出错了,更新失败";
    }

    /**
     * <p>
     * 审核成功 更新申请表 发卡
     * </p>
     *
     * @author liso
     * @since 2020/10/13 9:26
     * * @param cardProgress
     * * @return java.lang.String
     */

    @ApiOperation(value = "审核通过", notes = "同时发卡与生成初始账单")
    @ApiImplicitParam(name = "cardProgressId", value = "申请卡进程Id", required = true, paramType = "query", dataType
            = "Integer")
    @GetMapping("/adopt")
    @Transactional
    @ResponseBody
    public String adopt(Integer cardProgressId) {
        CardProgress cardProgress = new CardProgress();
        if (cardProgress != null) {
            cardProgress.setCardProgressId(cardProgressId);
        } else {
            return "没有业务号";
        }
        Date date = new Date();
        cardProgress.setCpDealTime(date);
        //1.获取subject和Seesion
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        Admin admin = (Admin) session.getAttribute("admin");
        cardProgress.setCpDealAdmin(admin.getId());
        cardProgress.setCpStatus("成功");
        if (iCardProgressService.examineUpdate(cardProgress) == 1) {

            if (iCardProgressService.insertCredit(cardProgressId) == 1) {
                return "发卡成功";
            }

        }
        return "出错了,发卡失败";
    }


    //去页面
    //转到信用卡申请查询页面
    @ApiOperation(value = "转到信用卡申请查询页面")
    @GetMapping("/goCardProgress")
    public ModelAndView goCardProgress() {
        return new ModelAndView("creditManager/cardprogress");
    }

    /**
     * 分页，查询办卡申请列表
     */
    @ApiOperation(value = "查询办卡申请列表", notes = "分页，查询办卡申请列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageIndex", value = "页码", required = true, paramType = "query", dataType
                    = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "数据条数", required = true, paramType = "query", dataType
                    = "Integer"),
            @ApiImplicitParam(name = "cardProgress", value = "申请卡进程对象", required = true, paramType = "query", dataType
                    = "Integer")
    })
    @GetMapping("/getCardProgressList")
    public PageResult<CardProgress> getCardProgressList(Integer pageIndex, Integer pageSize, CardProgress
            cardProgress) {
        pageIndex = pageIndex == null ? 1 : pageIndex;
        pageSize = pageSize == null ? 5 : pageSize;
        cardProgress = cardProgress == null ? new CardProgress() : cardProgress;
        return iCardProgressService.getCardProgressList(pageIndex, pageSize, cardProgress);
    }

    /**
     * 查询基本信息
     */
    @ApiOperation(value = "查询基本信息")
    @ApiImplicitParam(name = "essentialId", value = "基本信息Id", required = true, paramType = "query", dataType
            = "Integer")
    @GetMapping("/getEssential")
    public TEssential getEssential(Integer essentialId) {
        return itEssentialService.getTEssential(essentialId);
    }

    /**
     * 查询单位信息
     */
    @ApiOperation(value = "查询单位信息")
    @ApiImplicitParam(name = "uid", value = "单位信息Id", required = true, paramType = "query", dataType
            = "Integer")
    @GetMapping("/getUnit")


    public TUnitinfortion getUnit(Integer uid) {
        return itUnitinfortionService.getUnit(uid);

    }

    /**
     * 查询联系人信息
     */
    @ApiOperation(value = "查询联系人信息")
    @ApiImplicitParam(name = "contactId", value = "联系人Id", required = true, paramType = "query")
    @GetMapping("/getContact")
    public TContacts getContact(String contactId) {

        return itContactsService.getContact(contactId);
    }
}
