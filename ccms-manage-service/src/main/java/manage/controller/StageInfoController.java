package manage.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import commons.ResultBean;
import entity.CcmsCreditcardtype;
import entity.CcmsInstallment;
import entity.CcmsStage;
import manage.service.IStageInforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/StageInfor/viewDetails")
public class StageInfoController {
    @Autowired
    IStageInforService stageInforService;

//    订单详情数据
    @RequestMapping("/viewDetails")
    public String selectCcmsStageById(Integer ccmsInstallmentId ,Model model){
        List list = stageInforService.selectccmsStage(ccmsInstallmentId);
        //初始数据
        CcmsStage stage = (CcmsStage) list.get(0);
        //分期时间下拉框数据
        //初始数据
        model.addAttribute("stage",stage);
        //分期
        model.addAttribute("date",list);
        //分期申请id
        model.addAttribute("ccmsInstallmentId",ccmsInstallmentId);
        return "stageInfo";
    }

    @RequestMapping("/seleceByDate")
    @ResponseBody
    public String seleceByDate(Integer ccmsInstallmentId,String stageDate){
        CcmsStage stage = stageInforService.seleceByDate(ccmsInstallmentId,stageDate);
        String jsonString = JSON.toJSONString(stage);
        return jsonString;
    }
    @RequestMapping("/selectinstallment")
    public String selectinstallment(Integer installment){
        CcmsInstallment installment1 = stageInforService.selectInstallment(installment);
        return JSON.toJSONString(installment1);
    }


}
