package manage.mapper;

import entity.Admin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import vo.AdminVo;

import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
@Repository
public interface AdminMapper extends Mapper<Admin> {

    Set<String> queryPermsByAdminId(@Param("adminId") Integer adminId);

    Integer loadAdminMaxOrderNum();

}
