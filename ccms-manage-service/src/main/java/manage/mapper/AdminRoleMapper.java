package manage.mapper;

import entity.AdminRole;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
@Repository
public interface AdminRoleMapper extends Mapper<AdminRole> {

}
