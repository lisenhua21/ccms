package manage.mapper;

import entity.BillList;
import entity.CcmsBill;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Component
public interface BillMapper extends Mapper<CcmsBill> {
    @Select("SELECT b.`bill_id` billid,\n" +
            "b.`bill_create` billcreate,\n" +
            "(b.`bill_amount`+b.`bill_stage`) billmoney ,\n" +
            "b.`bill_status` billstatus,b.`bill_complete` billcomplete,\n" +
            "cr.`cardnum`,b.`bill_repaytime` billrepaytime,\n" +
            "b.`bill_produce` billproduce FROM ccms_bill b , \n" +
            "ccms_creditcard cr \n" +
            "WHERE b.`credit_id`=cr.`credit_id` AND b.`bill_status`='未完成' AND b.`bill_create`!='未出账单' AND cr.`cardnum` LIKE #{cardnum} AND b.`bill_create` LIKE #{month}")
    List<BillList> getBillByCardnum(@Param("cardnum") String cardnum, @Param("month") String month);
}
