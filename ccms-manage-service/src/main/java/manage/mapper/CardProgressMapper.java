package manage.mapper;

import entity.CardProgress;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface CardProgressMapper extends Mapper<CardProgress> {
}
