package manage.mapper;

import entity.CcmsRepayment;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;
@Component
public interface CcmsRepaymentMapper extends Mapper<CcmsRepayment> {

}
