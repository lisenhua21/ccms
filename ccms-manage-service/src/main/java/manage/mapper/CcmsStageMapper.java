package manage.mapper;

import entity.CcmsStage;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;
@Component
public interface CcmsStageMapper extends Mapper<CcmsStage> {

}
