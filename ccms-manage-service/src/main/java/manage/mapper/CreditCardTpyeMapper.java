package manage.mapper;

import entity.CcmsCreditcardtype;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface CreditCardTpyeMapper extends Mapper<CcmsCreditcardtype> {

    //根据卡号查询该卡等级和利率
    @Select("SELECT c2.three_stage threeStage,c2.six_stage sixStage,c2.nine_stage nineStage,c2.twelve_stage twelveStage,c2.eighteen_stage eighteenStage,c2.twentyfour_stage twentyfourStage" +
            " FROM ccms_creditcard c1,ccms_creditcardtype c2 WHERE c1.cardnum=#{cardnum} AND c1.credit_type=c2.typename")
    CcmsCreditcardtype selectInterestRate(String cardnum);
}