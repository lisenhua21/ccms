package manage.mapper;

import entity.CcmsInstallment;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;
@Component
public interface InstallmentMapper extends Mapper<CcmsInstallment> {
}
