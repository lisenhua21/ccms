package manage.mapper;

import entity.Permission;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
@Repository
public interface PermissionMapper extends Mapper<Permission> {

}
