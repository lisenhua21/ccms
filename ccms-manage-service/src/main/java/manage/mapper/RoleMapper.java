package manage.mapper;

import entity.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
@Repository
public interface RoleMapper extends Mapper<Role> {

    List<Integer> queryPermissionIdsByRoleId(Integer roleId);

    int deleteRolePermissionByRid(Integer rId);

    int saveRolePermission(@Param("rId") Integer rid, @Param("pIds") List<Integer> pIds);

    List<Integer> queryRoleIdsByAdminId(@Param("adminId") Integer adminId);

    int deleteRolesByAdminId(@Param("adminId") Integer adminId);

    int insertAdminRoles(@Param("adminId") Integer adminId,@Param("rIds") Integer[] rIds);

    Set<Integer> queryPermsByRoleIds(@Param("rIds") List<Integer> roleIds);
}
