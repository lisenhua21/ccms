package manage.service;


import commons.LayuiResult;
import commons.ResultBean;
import entity.Admin;
import vo.AdminVo;

import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
public interface AdminService {

    Admin login(String adminName);

    LayuiResult loadAllAdmins(AdminVo adminVo);

    Set<String> queryPermsByAdminId(Integer adminId);

    Integer loadAdminMaxOrderNum();

    int addAdmin(AdminVo adminVo);

    Admin queryByUsername(String username);

    int resetPassword(Integer id);

    int lock(Integer id);

    int unlock(Integer id);
}
