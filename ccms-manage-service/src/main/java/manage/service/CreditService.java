package manage.service;

import commons.LayuiResult;
import commons.ResultBean;
import entity.CcmsCredit;
import vo.UserCreditVo;

import java.util.List;

public interface CreditService {

    public LayuiResult getCredit4(UserCreditVo userCreditVo);

    public List<CcmsCredit> getCreditBycardnum(String cardnum);

    public CcmsCredit getCredit(long cardnum);

    public LayuiResult updateStatus(CcmsCredit ccmsCredit, int type);

    int updataActive(CcmsCredit c);

    public CcmsCredit getCreditById(Integer creditid);


    LayuiResult getCredit1(UserCreditVo userCreditVo);

    LayuiResult getCredit2(UserCreditVo userCreditVo);

    LayuiResult getCredit3(UserCreditVo userCreditVo);

    LayuiResult getCredit5(UserCreditVo userCreditVo);
}
