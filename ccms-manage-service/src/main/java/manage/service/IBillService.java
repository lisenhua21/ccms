package manage.service;

import commons.PageResult;
import entity.CcmsBill;

import java.math.BigDecimal;
import java.util.List;

public interface IBillService {
    List<CcmsBill> getAllBill(String creditId, String flag);

    CcmsBill getBill(String billCreate, String creitId);
    public CcmsBill getbillByPrimaryKey(int i);

    List<CcmsBill> getbillByCreditId(Integer creditId);

    int updateBill(Integer billId, BigDecimal money);
    public PageResult getBillByCardnum(int pageIndex, int pageSize, String cardnum, String month);
}
