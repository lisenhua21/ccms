package manage.service;

import commons.PageResult;
import entity.CardProgress;


public interface ICardProgressService {
    PageResult<CardProgress> getCardProgressList(Integer pageIndex, Integer pageSize, CardProgress cardProgress);

    int insertProgress(CardProgress cardProgress);

    int examineUpdate(CardProgress cardProgress);


    int insertCredit(Integer cpId);
}
