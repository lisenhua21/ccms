package manage.service;

import entity.CcmsInstallment;
import entity.CcmsStage;

import java.util.List;

public interface IStageInforService {
    List selectccmsStage(Integer ccmsInstallmentId);

    CcmsStage seleceByDate(Integer ccmsInstallmentId, String stageDate);

    CcmsInstallment selectInstallment(Integer installment);
}
