package manage.service;

import entity.TContacts;

public interface ITContactsService {
    //查询联系人详情
    TContacts getContact(String contactId);
}
