package manage.service;

import entity.TEssential;

public interface ITEssentialService {
    public TEssential getUser(String identityId);

    public TEssential getUserByKey(int userId);
    //基本信息详细查询
    TEssential getTEssential(Integer essentialId);
}
