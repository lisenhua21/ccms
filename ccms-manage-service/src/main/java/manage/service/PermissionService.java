package manage.service;


import commons.LayuiResult;
import vo.PermissionVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
public interface PermissionService {

    LayuiResult loadMenuJson();

    LayuiResult loadLeftMenuJson();

    LayuiResult page(PermissionVo permissionVo);

    LayuiResult loadPermissionLeftDTreeJson();

    LayuiResult loadPermissions(PermissionVo permissionVo);

    LayuiResult initRolePermissionsByRoleId(Integer roleId);
}
