package manage.service;

import commons.LayuiResult;
import commons.ResultBean;
import vo.RoleVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
public interface RoleService {

    LayuiResult loadAllRoles(RoleVo roleVo);

    int saveRolePermission(Integer rId, Integer[] pIds);

    LayuiResult initRoleByAdminId(Integer adminId);

    int saveUserRole(Integer adminId, Integer[] rIds);
}
