package manage.service.impl;


import cn.hutool.core.util.IdUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import commons.LayuiResult;
import commons.ResultBean;
import entity.Admin;
import manage.mapper.AdminMapper;
import manage.service.AdminService;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import vo.AdminVo;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin login(String adminName) {
        Example example = new Example(Admin.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("username",adminName);
        Admin admin = adminMapper.selectOneByExample(example);
        return admin;
    }

    @Override
    public LayuiResult loadAllAdmins(AdminVo adminVo) {
        PageHelper.startPage(adminVo.getPage(),adminVo.getLimit());
        Example example = new Example(Admin.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("type",1);
        List<Admin> list = adminMapper.selectByExample(example);
        Page pList = (Page) list;
        LayuiResult layuiResult = new LayuiResult(pList.getTotal(), pList.getResult());
        return layuiResult;
    }

    /**
     * 根据管理员ID查询所拥有的权限
     * @param adminId
     * @return
     */
    @Override
    public Set<String> queryPermsByAdminId(Integer adminId) {
        Set<String> set = adminMapper.queryPermsByAdminId(adminId);
        return set;
    }

    @Override
    public Integer loadAdminMaxOrderNum() {
        return adminMapper.loadAdminMaxOrderNum();
    }

    @Override
    public int addAdmin(AdminVo adminVo) {
        return adminMapper.insert(adminVo);
    }

    @Override
    public Admin queryByUsername(String username) {
        Example example = new Example(Admin.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andLike("username","%"+username+"%");
        Admin admin = adminMapper.selectOneByExample(example);
        return admin;
    }

    /**
     * 重置管理员密码
     * @param id
     * @return
     */
    @Override
    public int resetPassword(Integer id) {
        Admin admin = new Admin();
        admin.setId(id);
        //设置新的盐
        String salt= IdUtil.simpleUUID().toUpperCase();
        admin.setSalt(salt);
        //设置新的密码为6个1,散列1次
        String password = new SimpleHash("MD5", "111111", salt, 1).toString();
        admin.setPassword(password);
        int update = adminMapper.updateByPrimaryKeySelective(admin);
        return update;
    }

    /**
     * 锁定管理员账户
     * @param id
     * @return
     */
    @Override
    public int lock(Integer id) {
        Admin admin = new Admin();
        admin.setId(id);
        admin.setAvailable(-1);
        int update = adminMapper.updateByPrimaryKeySelective(admin);
        return update;
    }

    /**
     * 解锁管理员
     * @param id
     * @return
     */
    @Override
    public int unlock(Integer id) {
        Admin admin = new Admin();
        admin.setId(id);
        admin.setAvailable(1);
        int update = adminMapper.updateByPrimaryKeySelective(admin);
        return update;
    }
}
