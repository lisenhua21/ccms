package manage.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import commons.PageResult;
import entity.BillList;
import entity.CcmsBill;
import manage.mapper.BillMapper;
import manage.service.IBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class BillServiceImpl implements IBillService {

    @Autowired
    private BillMapper mapper;
    @Autowired
    private StringRedisTemplate template;

    @Override
    public List<CcmsBill> getAllBill(String creditId, String flag) {
        String BillList = "";
        List<CcmsBill> ccmsBills = null;
            //第一次查询，从数据库查
            Example example = new Example(CcmsBill.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("creditId",creditId);
            example.setOrderByClause("bill_create desc");
            ccmsBills = mapper.selectByExample(example);
            if (flag == null){
                //如果flag为null则为已出账单查询，否则为未出账单查询
                CcmsBill undoneCcmsBill = ccmsBills.get(0);
                ccmsBills.remove(0);
                ccmsBills.add(undoneCcmsBill);
            }
        return ccmsBills;
    }

    @Override
    public CcmsBill getBill(String billCreate, String creditId) {
        Example example = new Example(CcmsBill.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("billCreate",billCreate);
        criteria.andEqualTo("creditId",creditId);
        return mapper.selectOneByExample(example);
    }

    @Override
    public CcmsBill getbillByPrimaryKey(int i) {
        return mapper.selectByPrimaryKey(i);
    }

    @Override
    public List<CcmsBill> getbillByCreditId(Integer creditId) {
        Example exmaple = new Example(CcmsBill.class);
        Example.Criteria criteria = exmaple.createCriteria();
        criteria.andEqualTo("creditId",creditId);
        List<CcmsBill> ccmsBills = mapper.selectByExample(exmaple);
        return ccmsBills;
    }
    //银行卡还款完成修改账单金额
    @Override
    public int updateBill(Integer billId, BigDecimal money) {
        CcmsBill bill = mapper.selectByPrimaryKey(billId);
        BigDecimal billillAmount=BigDecimal.valueOf(bill.getBillAmount());
        //先还BillAmount消费账单,再还分期账单
        if (money.compareTo(billillAmount)!=-1){ //还款的钱大于等于BillAmount
            BigDecimal subtract = money.subtract(billillAmount); //还款的钱减去BillAmount剩余还款的
            bill.setBillAmount(0.00);
            double balance =  bill.getBillStage()-subtract.doubleValue();
            bill.setBillStage(balance);
            if (balance==0){
                bill.setBillStatus("已完成");//
                Date date = new Date();
                SimpleDateFormat sf= new SimpleDateFormat("yyyy-MM-dd");
                String format = sf.format(date);
                bill.setBillComplete(format);//完成时间
                bill.setBillType("结清");
            }
        }else{                                      //还款的钱小于BillAmount
            BigDecimal subtract = billillAmount.subtract(money);
            bill.setBillAmount(subtract.doubleValue());
        }
        int i = mapper.updateByPrimaryKeySelective(bill);
        return i;
    }

    @Override
    public PageResult getBillByCardnum(int pageIndex, int pageSize, String cardnum, String month) {
        PageHelper.startPage(pageIndex,pageSize);
        String month1 = month.replace("年", "-");
        String month2 = month1.replace("月", "-");
        List<BillList> list = mapper.getBillByCardnum("%"+cardnum+"%","%"+month2+"%");
        Page<BillList> plist = (Page<BillList>) list;
        PageResult pb=new PageResult(plist.getPageNum(),plist.getPageSize(),plist.getTotal(),plist.getPages(),plist.getResult());
        return pb;
    }
}
