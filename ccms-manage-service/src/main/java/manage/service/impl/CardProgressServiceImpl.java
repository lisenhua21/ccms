package manage.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import commons.PageResult;
import entity.CardProgress;
import entity.CcmsBill;
import entity.CcmsCredit;
import entity.CcmsCreditcardtype;
import lombok.AllArgsConstructor;
import manage.mapper.BillMapper;
import manage.mapper.CardProgressMapper;
import manage.mapper.CreditCardTpyeMapper;
import manage.mapper.CreditMapper;
import manage.service.ICardProgressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;

@Transactional
@Service
public class CardProgressServiceImpl implements ICardProgressService {
    @Autowired
    CardProgressMapper cardProgressMapper;
    //卡片类型mapper
    @Autowired
    private CreditCardTpyeMapper creditCardTpyeMapper;
    @Autowired
    private CreditMapper creditMapper;
    @Autowired
    private BillMapper mapper;

    public PageResult<CardProgress> getCardProgressList(Integer pageIndex, Integer pageSize, CardProgress
            cardProgress) {
        Example example = new Example(CardProgress.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("creditType", cardProgress.getCreditType());
        criteria.andEqualTo("cpStatus", "申请中");
        PageHelper.startPage(pageIndex, pageSize);
        Page<CardProgress> page = (Page<CardProgress>) cardProgressMapper.selectByExample(example);
        return new PageResult<>(
                page.getPageNum(),
                page.getPageSize(),
                page.getTotal(),
                page.getPages(),
                page.getResult());
    }

    @Override
    public int insertProgress(CardProgress cardProgress) {

        return cardProgressMapper.insert(cardProgress);
    }

    @Override
    public int examineUpdate(CardProgress cardProgress) {
        return cardProgressMapper.updateByPrimaryKeySelective(cardProgress);

    }


    /**
     * <p>
     * 生成卡片
     * </p>
     * * <p>
     * * 卡片初始模板:
     * * cardnum     卡号        生成方式:random
     * * creditType  类型        typeid
     * * actve       未激活      固定值
     * * status      正常        固定值
     * * creditLimit 类型卡额度  typeid
     * * debt        0欠款       固定值
     * * essentialid 基本信息id  essential
     * * creditPwd   888888     初始密码固定
     * * remindTime  1          默认提醒时间
     * * userId      所属用户id  userid
     * * </p>
     *
     * @author liso
     * @since 2020/10/13 17:26
     * * @param
     * * @return int
     */
    @Override
    public int insertCredit(Integer cpId) {
        // 获取申请信息
        CardProgress cardProgress = cardProgressMapper.selectByPrimaryKey(cpId);
        CcmsCredit credit = new CcmsCredit();
        //设置初始密码为六个8
        credit.setCreditPwd("888888");
        //基本信息ID
        credit.setEssentialId(cardProgress.getEssentialid());
        //设置欠款
        credit.setDebt(BigDecimal.valueOf(0));
        //设置初始状态
        credit.setStatus("正常");
        //设置用户ID
        Integer cpCreateUserid = cardProgress.getCpCreateUserid();
        credit.setUserId(cpCreateUserid);
        //状态为未激活
        credit.setActive("未激活");
        //预存款默认为1000
        credit.setDeposit(BigDecimal.valueOf(1000.00));
        //生成卡号
        int num = (int) (Math.random() * 1000000);
        int num2 = (int) (Math.random() * 1000000);
        String Cardnum = "123" + num + num2;
        credit.setCardnum(Long.parseLong(Cardnum));
        //默认提醒还款时间为每月一号
        credit.setRemindTime("1");


        //查询申请的卡片类型并发卡
        Example exm = new Example(CcmsCreditcardtype.class);
        Example.Criteria criteria = exm.createCriteria();
        criteria.andEqualTo("typename", cardProgress.getCreditType());
        CcmsCreditcardtype ccmsCreditcardtype = creditCardTpyeMapper.selectOneByExample(exm);//查询
        //写入卡片信息
        credit.setCreditLimit(BigDecimal.valueOf(ccmsCreditcardtype.getCardLimit()));
        credit.setCreditType(cardProgress.getCreditType());


        int insert = creditMapper.insert(credit);
        //发卡的同时生成账单
        if (insert == 1) {
            mapper.insertSelective(new CcmsBill(null, "未出账单", 0.00, 0.00, "未完成", "", "待操作", credit.getCreditId(), null,
                    "短信"));
        }
        return insert;

    }

}
