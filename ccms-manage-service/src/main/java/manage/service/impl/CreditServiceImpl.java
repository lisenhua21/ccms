package manage.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import commons.LayuiResult;
import commons.ResultBean;
import entity.CcmsCredit;
import entity.TEssential;
import entity.UserCredit;
import manage.mapper.CreditCardTpyeMapper;
import manage.mapper.CreditMapper;
import manage.mapper.TEssentialMapper;
import manage.service.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import vo.UserCreditVo;

import java.util.ArrayList;
import java.util.List;

@Service
public class CreditServiceImpl implements CreditService {

    @Autowired
    CreditMapper creditMapper;

    @Autowired
    TEssentialMapper tEssentialMapper;

    /**
     * 需要激活的卡
     *
     * @return
     */
    @Override
    public LayuiResult getCredit4(UserCreditVo userCreditVo) {
        PageHelper.startPage(userCreditVo.getPage(), userCreditVo.getLimit());
        Example example = new Example(CcmsCredit.class);
        Example.Criteria criteria = example.createCriteria();
        if (userCreditVo.getCardnum() != 0) {
            criteria.andEqualTo("cardnum", userCreditVo.getCardnum());
        } else {
            criteria.andEqualTo("active", "待激活");
        }
        Page<CcmsCredit> page = (Page<CcmsCredit>) creditMapper.selectByExample(example);
        List<UserCredit> list1 = new ArrayList<>();
        for (CcmsCredit s : page.getResult()) {
            TEssential te = tEssentialMapper.selectByPrimaryKey(s.getEssentialId());
            UserCredit uc = new UserCredit(te.getIdentityid(), s.getCardnum(), te.getRealname(), s.getActive(), s
                    .getStatus(), te.getPhonenumber());
            list1.add(uc);
        }
        LayuiResult layuiResult = new LayuiResult(page.getTotal(), list1);
        return layuiResult;
    }

    /**
     * 管理员激活单个用户
     *
     * @param cardnum
     * @return
     */
    @Override
    public List<CcmsCredit> getCreditBycardnum(String cardnum) {
        Example example = new Example(CcmsCredit.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("cardnum", cardnum);
        return creditMapper.selectByExample(example);
    }

    @Override
    public CcmsCredit getCredit(long cardnum) {
        Example example = new Example(CcmsCredit.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("cardnum", cardnum);
        CcmsCredit ccmsCredit = creditMapper.selectOneByExample(example);
        return ccmsCredit;
    }

    @Override
    public LayuiResult updateStatus(CcmsCredit ccmsCredit, int type) {
        switch (type) {
            case 1:
                ccmsCredit.setStatus("正常");
                break;
            case 2:
                ccmsCredit.setStatus("申请挂失");
                break;
            case 3:
                ccmsCredit.setStatus("已挂失");
                break;
            case 4:
                ccmsCredit.setStatus("申请冻结");
                break;
            case 5:
                ccmsCredit.setStatus("已冻结");
                break;
            case 6:
                ccmsCredit.setStatus("申请注销");
                break;
            case 7:
                ccmsCredit.setStatus("已注销");
                break;
        }
        int s = creditMapper.updateByPrimaryKey(ccmsCredit);
        if (s > 0) {
            return new LayuiResult("修改成功");
        } else {
            return new LayuiResult("修改失败");
        }
    }

    @Override
    public int updataActive(CcmsCredit c) {
        return creditMapper.updateByPrimaryKey(c);
    }

    @Override
    public LayuiResult getCredit1(UserCreditVo userCreditVo) {
        PageHelper.startPage(userCreditVo.getPage(), userCreditVo.getLimit());
        Example example = new Example(CcmsCredit.class);
        Example.Criteria criteria = example.createCriteria();
        if (userCreditVo.getCardnum() != 0) {
            criteria.andEqualTo("cardnum", userCreditVo.getCardnum());
        } else {
            criteria.andEqualTo("status", "申请挂失");
        }
        Page<CcmsCredit> page = (Page<CcmsCredit>) creditMapper.selectByExample(example);
        List<UserCredit> list1 = new ArrayList<>();
        for (CcmsCredit s : page.getResult()) {
            TEssential te = tEssentialMapper.selectByPrimaryKey(s.getEssentialId());
            UserCredit uc = new UserCredit(te.getIdentityid(), s.getCardnum(), te.getRealname(), s.getActive(), s
                    .getStatus(), te.getPhonenumber());
            list1.add(uc);
        }
        LayuiResult layuiResult = new LayuiResult(page.getTotal(), list1);
        return layuiResult;
    }

    @Override
    public LayuiResult getCredit2(UserCreditVo userCreditVo) {
        PageHelper.startPage(userCreditVo.getPage(), userCreditVo.getLimit());
        Example example = new Example(CcmsCredit.class);
        Example.Criteria criteria = example.createCriteria();
        if (userCreditVo.getCardnum() != 0) {
            criteria.andEqualTo("cardnum", userCreditVo.getCardnum());
        } else {
            criteria.andEqualTo("status", "申请冻结");
        }
        Page<CcmsCredit> page = (Page<CcmsCredit>) creditMapper.selectByExample(example);
        List<UserCredit> list1 = new ArrayList<>();
        for (CcmsCredit s : page.getResult()) {
            TEssential te = tEssentialMapper.selectByPrimaryKey(s.getEssentialId());
            UserCredit uc = new UserCredit(te.getIdentityid(), s.getCardnum(), te.getRealname(), s.getActive(), s
                    .getStatus(), te.getPhonenumber());
            list1.add(uc);
        }
        LayuiResult layuiResult = new LayuiResult(page.getTotal(), list1);
        return layuiResult;
    }

    @Override
    public LayuiResult getCredit3(UserCreditVo userCreditVo) {
        PageHelper.startPage(userCreditVo.getPage(), userCreditVo.getLimit());
        Example example = new Example(CcmsCredit.class);
        Example.Criteria criteria = example.createCriteria();
        if (userCreditVo.getCardnum() != 0) {
            criteria.andEqualTo("cardnum", userCreditVo.getCardnum());
        } else {
            criteria.andEqualTo("status", "申请注销");
        }
        Page<CcmsCredit> page = (Page<CcmsCredit>) creditMapper.selectByExample(example);
        List<UserCredit> list1 = new ArrayList<>();
        for (CcmsCredit s : page.getResult()) {
            TEssential te = tEssentialMapper.selectByPrimaryKey(s.getEssentialId());
            UserCredit uc = new UserCredit(te.getIdentityid(), s.getCardnum(), te.getRealname(), s.getActive(), s
                    .getStatus(), te.getPhonenumber());
            list1.add(uc);
        }
        LayuiResult layuiResult = new LayuiResult(page.getTotal(), list1);
        return layuiResult;
    }

    @Override
    public LayuiResult getCredit5(UserCreditVo userCreditVo) {
        PageHelper.startPage(userCreditVo.getPage(), userCreditVo.getLimit());
        Page<CcmsCredit> page = null;
        if (userCreditVo.getCardnum() != 0) {
            Example example = new Example(CcmsCredit.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("cardnum", userCreditVo.getCardnum());
            page = (Page<CcmsCredit>) creditMapper.selectByExample(example);
        } else {
            page = (Page<CcmsCredit>) creditMapper.selectAll();
        }
        List<UserCredit> list1 = new ArrayList<>();
        for (CcmsCredit s : page.getResult()) {
            TEssential te = tEssentialMapper.selectByPrimaryKey(s.getEssentialId());
            UserCredit uc = new UserCredit(te.getIdentityid(), s.getCardnum(), te.getRealname(), s.getActive(), s
                    .getStatus(), te.getPhonenumber());
            list1.add(uc);
        }
        LayuiResult layuiResult = new LayuiResult(page.getTotal(), list1);
        return layuiResult;
    }

    @Override
    public CcmsCredit getCreditById(Integer creditid) {
        return creditMapper.selectByPrimaryKey(creditid);
    }



}
