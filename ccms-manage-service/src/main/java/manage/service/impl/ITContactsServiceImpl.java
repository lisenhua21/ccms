package manage.service.impl;

import entity.TContacts;
import manage.mapper.TContactsMapper;
import manage.service.ITContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ITContactsServiceImpl implements ITContactsService {
    @Autowired
    TContactsMapper tContactsMapper;
    @Override
    public TContacts getContact(String contactId) {
        return tContactsMapper.getContact(contactId);
    }
}
