package manage.service.impl;

import entity.TUnitinfortion;
import manage.mapper.TUnitinfortionMapper;
import manage.service.ITUnitinfortionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ITUnitinfortionServiceImpl implements ITUnitinfortionService {
    @Autowired
    TUnitinfortionMapper tUnitinfortionMapper;
    @Override
    public TUnitinfortion getUnit(Integer uid) {
        return tUnitinfortionMapper.getUnit(uid);
    }
}
