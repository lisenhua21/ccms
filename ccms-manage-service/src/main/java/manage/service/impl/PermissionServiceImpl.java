package manage.service.impl;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import commons.LayuiResult;
import commons.TreeNode;
import commons.TreeNodeBuilder;
import entity.Admin;
import entity.Permission;
import manage.mapper.PermissionMapper;
import manage.mapper.RoleMapper;
import manage.service.PermissionService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import vo.PermissionVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private RoleMapper roleMapper;

    /**
     * 加载菜单列表
     * @return
     */
    @Override
    public LayuiResult loadMenuJson() {
        Example example = new Example(Permission.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("type","menu");
        criteria.andEqualTo("available",1);
        List<Permission> list = new ArrayList<>();
        Session session = SecurityUtils.getSubject().getSession();
        Admin admin = (Admin) session.getAttribute("admin");
        //根据用户权限查出该用户所拥有的菜单
        if (admin.getType() == 0) {
            //如果是超级管理员拥有所有菜单
            list = permissionMapper.selectByExample(example);
        } else {
            Integer id = admin.getId();
            //通过用户查出该用户所拥有的角色
            List<Integer> roleIds = roleMapper.queryRoleIdsByAdminId(id);
            //判断角色Id是否为空
            if (roleIds.size() == 0){
                return new LayuiResult(null);
            }
            //查出所有角色的所有的菜单权限
            Set<Integer> perms = roleMapper.queryPermsByRoleIds(roleIds);
            if (perms.size() > 0) {
                criteria.andIn("id",perms);
                list = permissionMapper.selectByExample(example);
            } else {
                list = null;
            }
        }
        //查出所有菜单
        //List<Permission> list = permissionMapper.selectByExample(example);
        //创建节点的list集合
        if (null == list){
            return new LayuiResult(null);
        }
        ArrayList<TreeNode> treeNodes = new ArrayList<>();
        for (Permission p : list) {
            Integer id = p.getId();
            Integer pid = p.getPid();
            String title = p.getTitle();
            String icon = p.getIcon();
            String href = p.getHref();
            Boolean spread = p.getOpen() == 1 ? true : false;
            treeNodes.add(new TreeNode(id, pid, title, icon, href, spread));
        }
        List<TreeNode> nodes = TreeNodeBuilder.build(treeNodes, 1);
        LayuiResult layResult = new LayuiResult(nodes);
        return layResult;
    }

    /**
     * 加载菜单管理页面左侧的Json树
     * @return
     */
    @Override
    public LayuiResult loadLeftMenuJson() {
        Example example = new Example(Permission.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("type","menu");
        criteria.andEqualTo("available",1);
        //查出所有菜单
        List<Permission> list = permissionMapper.selectByExample(example);
        List<TreeNode> treeNodes = new ArrayList<>();
        for (Permission menu : list) {
            Boolean spread = menu.getOpen() == 1 ? true : false;
            treeNodes.add(new TreeNode(menu.getId(), menu.getPid(), menu.getTitle(), spread));
        }
        LayuiResult layuiResult = new LayuiResult(treeNodes);
        return layuiResult;
    }

    /**
     * 根据条件加载菜单表
     * @param permissionVo
     */
    @Override
    public LayuiResult page(PermissionVo permissionVo) {
        //按条件进行分页
        PageHelper.startPage(permissionVo.getPage(),permissionVo.getLimit());
        //查看是否有查询条件
        Example example = new Example(Permission.class);
        Example.Criteria criteria = example.createCriteria();
        if (permissionVo.getId() != null){
            criteria.andEqualTo("id",permissionVo.getId());
            criteria.orEqualTo("pid",permissionVo.getId());
        }
        criteria.andEqualTo("type","menu");
        List<Permission> list = permissionMapper.selectByExample(example);
        Page pList = (Page) list;
        LayuiResult layuiResult = new LayuiResult(pList.getTotal(),pList.getResult());
        return layuiResult;
    }

    /**
     * 加载权限页面左边的权限树
     * @return
     */
    @Override
    public LayuiResult loadPermissionLeftDTreeJson() {
        Example example = new Example(Permission.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("type","menu");
        criteria.andEqualTo("available",1);
        //查出所有菜单
        List<Permission> list = permissionMapper.selectByExample(example);
        List<TreeNode> treeNodes = new ArrayList<>();
        for (Permission menu : list) {
            Boolean spread = menu.getOpen() == 1 ? true : false;
            treeNodes.add(new TreeNode(menu.getId(), menu.getPid(), menu.getTitle(), spread));
        }
        LayuiResult layuiResult = new LayuiResult(treeNodes);
        return layuiResult;
    }

    /**
     * 加载所有权限或通过点击权限页面左侧权限树加载该菜单下的权限
     * @param permissionVo
     * @return
     */
    @Override
    public LayuiResult loadPermissions(PermissionVo permissionVo) {
        PageHelper.startPage(permissionVo.getPage(),permissionVo.getLimit());
        Example example = new Example(Permission.class);
        Example.Criteria criteria = example.createCriteria();
        if (permissionVo.getId() != null){
            criteria.andEqualTo("pid",permissionVo.getId());
        }
        criteria.andEqualTo("type","permission");
        List<Permission> list = permissionMapper.selectByExample(example);
        Page pList = (Page) list;
        LayuiResult layuiResult = new LayuiResult(pList.getTotal(),pList.getResult());
        return layuiResult;
    }

    /**
     * 加载某个角色下的所有权限
     * @param roleId
     * @return
     */
    @Override
    public LayuiResult initRolePermissionsByRoleId(Integer roleId) {
        Example example = new Example(Permission.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("available",1);
        List<Permission> permissions = permissionMapper.selectByExample(example);
        //查询该角色所拥有的所有菜单和权限
        //该角色拥有的菜单和权限id的集合
        List<Integer> ownPermissions = roleMapper.queryPermissionIdsByRoleId(roleId);
        //构造 List<TreeNode>
        List<TreeNode> nodes = new ArrayList<>();
        for (Permission p : permissions) {
            String checkArr = "0";
            for (Integer pid : ownPermissions) {
                if (p.getId() == pid) {
                    checkArr = "1";
                    break;
                }
            }
            nodes.add(new TreeNode(p.getId(), p.getPid(), p.getTitle(), checkArr));
        }
        LayuiResult layuiResult = new LayuiResult(0,"",nodes);
        return layuiResult;
    }
}
