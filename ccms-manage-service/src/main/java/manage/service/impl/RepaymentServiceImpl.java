package manage.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import commons.PageResult;
import entity.CcmsRepayment;
import manage.mapper.CcmsRepaymentMapper;
import manage.service.IRepaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class RepaymentServiceImpl implements IRepaymentService {
    @Autowired
    CcmsRepaymentMapper repaymentMapper;
    @Override
    public List<CcmsRepayment> getAllRepayment() {
        return repaymentMapper.selectAll();
    }

    @Override
    public int addRepayment(CcmsRepayment repayment) {
        int n=0;
        if (repayment.getCardnum()!=null&&repayment.getBankcardnum()!=null){
            n = repaymentMapper.insert(repayment);
        }
        return n;
    }
    //根据userid查询还款明细返回pagebean
    @Override
    public PageResult getpages(int pageIndex, int pageSize, String cardnum,String date) {
        PageHelper.startPage(pageIndex,pageSize);
        if (cardnum==""){
            cardnum=null;
        }
        Example example = new Example(CcmsRepayment.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andLike("cardnum",cardnum);
        if (date!=null&&!date.equals("")){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date1 = sdf.parse(date);
                Date date2= new Date(date1.getTime()+1*24*60*60*1000);
                criteria.andBetween("repaymentDate",date1,date2);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        example.setOrderByClause("repayment_date desc");
        List<CcmsRepayment> list= repaymentMapper.selectByExample(example);
        Page<CcmsRepayment> plist = (Page<CcmsRepayment>) list;
        PageResult pb=new PageResult(plist.getPageNum(),plist.getPageSize(),plist.getTotal(),plist.getPages(),plist.getResult());
        return pb;
    }
}

