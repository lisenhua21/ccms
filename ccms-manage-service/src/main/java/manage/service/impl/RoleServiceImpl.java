package manage.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import commons.LayuiResult;
import commons.ResultBean;
import entity.Permission;
import entity.Role;
import manage.mapper.RoleMapper;
import manage.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import vo.RoleVo;

import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Rob1n
 * @since 2020-09-22
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public LayuiResult loadAllRoles(RoleVo roleVo) {
        PageHelper.startPage(roleVo.getPage(),roleVo.getLimit());
        Example example = new Example(Role.class);
        Example.Criteria criteria = example.createCriteria();
        List<Role> list = roleMapper.selectByExample(example);
        Page pList = (Page) list;
        LayuiResult layuiResult = new LayuiResult(pList.getTotal(), pList.getResult());
        return layuiResult;
    }

    /**
     * 保存角色和权限的关系
     * @param rId
     * @param pIds
     * @return
     */
    @Override
    public int saveRolePermission(Integer rId, Integer[] pIds) {
        //先删除原有的权限id
        roleMapper.deleteRolePermissionByRid(rId);
        //再保存新增的权限id
        List<Integer> list = Arrays.asList(pIds);
        return roleMapper.saveRolePermission(rId, list);
    }

    /**
     * 加载所有角色以及用户所拥有的角色
     * @param adminId
     * @return
     */
    @Override
    public LayuiResult initRoleByAdminId(Integer adminId) {
        //查询所有可用的角色
        Example example = new Example(Permission.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("available",1);
        List<Role> roles = roleMapper.selectByExample(example);
        //将返回的结果集循环遍历封装进map中
        List<Map<String,Object>> list = new ArrayList<>();
        for (Role role : roles) {
            Map<String,Object> map = new HashMap<>();
            map.put("id",role.getId());
            map.put("name",role.getName());
            map.put("remark",role.getRemark());
            map.put("available",role.getAvailable());
            list.add(map);
        }
        System.out.println(list);
        //查询当前用户所拥有的角色
        List<Integer> myRoles = roleMapper.queryRoleIdsByAdminId(adminId);
        //遍历角色集合
        for (Map<String, Object> lMap : list) {
            Boolean LAY_CHECKED = false;
            Integer roleId = (Integer) lMap.get("id");
            for (Integer rid : myRoles) {
                if (rid == roleId) {
                    LAY_CHECKED = true;
                    break;
                }
            }
            lMap.put("LAY_CHECKED", LAY_CHECKED);
        }
        LayuiResult layuiResult = new LayuiResult(Long.valueOf(list.size()), list);
        return layuiResult;
    }

    /**
     * 保存用户和角色的关系
     * @param adminId
     * @param rIds
     * @return
     */
    @Override
    public int saveUserRole(Integer adminId, Integer[] rIds) {
        //先删除该用户原有的角色
        int rows = roleMapper.deleteRolesByAdminId(adminId);
        //保存角色
        return roleMapper.insertAdminRoles(adminId, rIds);
    }
}
