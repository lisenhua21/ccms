package manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import commons.ResultBean;
import entity.CcmsCreditcardtype;
import entity.CcmsInstallment;
import entity.CcmsStage;
import manage.mapper.CcmsStageMapper;
import manage.mapper.InstallmentMapper;
import manage.service.IStageInforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class StageInforService implements IStageInforService {


    @Autowired
    CcmsStageMapper ccmsStageMapper;
    @Autowired
    InstallmentMapper installmentMapper;
    @Autowired
    private StringRedisTemplate template;

    @Override
    public List selectccmsStage(Integer ccmsInstallmentId) {
        String stageString = template.opsForValue().get(ccmsInstallmentId+"Stages");
        List<CcmsStage> stages = new ArrayList();
        if (stageString==null){
            CcmsStage ccmsStage = new CcmsStage();
            ccmsStage.setCcmsInstallmentId(ccmsInstallmentId);
            stages = ccmsStageMapper.select(ccmsStage);
            String jsonString = JSON.toJSONString(stages);
            template.opsForValue().set(ccmsInstallmentId+"Stages",jsonString);
            return stages;
        }else {
            stages = JSONArray.parseArray(stageString,CcmsStage.class);
        }
        return stages;
    }

    @Override
    public CcmsStage seleceByDate(Integer ccmsInstallmentId, String stageDate) {
        List<CcmsStage> list = selectccmsStage(ccmsInstallmentId);
        for (CcmsStage stage : list) {
            if (stage.equals(stageDate)){
                return stage;
            }
        }
        return null;
    }

    @Override
    public CcmsInstallment selectInstallment(Integer installment) {
        CcmsInstallment ccmsInstallment = installmentMapper.selectByPrimaryKey(installment);
        return ccmsInstallment;
    }


}
