package manage.service.impl;

import entity.TEssential;
import manage.mapper.TEssentialMapper;
import manage.service.ITEssentialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class TEssentialServiceImlp implements ITEssentialService {
    @Autowired
    TEssentialMapper tEssentialMapper;



    @Override
    public TEssential getUser(String identityId) {
        Example example=new Example(TEssential.class);
        Example.Criteria criteria=example.createCriteria();
        criteria.andEqualTo("identityid",identityId);
        return tEssentialMapper.selectOneByExample(example);
    }

    @Override
    public TEssential getUserByKey(int userId) {
        return tEssentialMapper.selectByPrimaryKey(userId);
    }

    @Override
    public TEssential getTEssential(Integer essentialId) {
        return tEssentialMapper.getTEssential(essentialId);
    }
}
