package user.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import user.interceptor.LoginInterceptor;

@Configuration
public class MyWebConfig implements WebMvcConfigurer {

    @Autowired
    private LoginInterceptor loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //登录拦截器
        registry.addInterceptor(loginInterceptor)
                .excludePathPatterns("/static/**")
                .excludePathPatterns("/users/**")
                .excludePathPatterns("/tologin")
                .addPathPatterns("/exitToLogin")
                .addPathPatterns("/")
                .addPathPatterns("/index")
                .addPathPatterns("/shenqin/**")
                .addPathPatterns("/bill/**")
                .addPathPatterns("/billType/**")
                .addPathPatterns("/repayment/**")
                .addPathPatterns("/creditType/**")
                .addPathPatterns("/credit/**")
                .addPathPatterns("/installment/**")
                .addPathPatterns("/progress/**")
                .addPathPatterns("/selected/**");
    }

}
