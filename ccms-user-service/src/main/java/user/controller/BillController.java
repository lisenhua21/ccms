package user.controller;

import entity.CcmsCreditcardtype;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import user.service.IBillService;
import commons.ResultBean;
import entity.CcmsBill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/bill")
public class BillController {

    @Autowired
    private IBillService iBillService;

    @Autowired
    private StringRedisTemplate template;

    @RequestMapping("/goBill")
    public String goBill(){
        return "bill";
    }

    @RequestMapping("/goUser")
    public String goUser(){
        return "user";
    }

    @RequestMapping("/list")
    public String getAllBillByid(String flag,Model model,HttpServletRequest request){
        Integer userId =  Integer.parseInt((String)request.getSession().getAttribute("userid"));
        String creditId = template.opsForValue().get("creditId"+userId);
        List<CcmsBill> allBill = iBillService.getAllBill(creditId,flag);
        CcmsBill thisBill = allBill.get(0);
        double sumBill = thisBill.getBillStage()+thisBill.getBillAmount();
        double minBill = sumBill/10;
        model.addAttribute("sumBill",sumBill);
        model.addAttribute("minBill",minBill);
        model.addAttribute("allBill",allBill);
        model.addAttribute("thisBill",thisBill);
        return "bill";
    }
    @ResponseBody
    @RequestMapping("/selectBill")
    public ResultBean<CcmsBill> selectBill(String billCreate, HttpServletRequest request){
        Integer userId =  Integer.parseInt((String)request.getSession().getAttribute("userid"));
        String creditId = template.opsForValue().get("creditId"+userId);
        return new ResultBean<>(0,"查询成功!!!",iBillService.getBill(billCreate,creditId));
    }

}
