package user.controller;

import entity.CcmsBilltype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import user.service.IBilltypeService;

import java.util.List;

@Controller
@RequestMapping("/billType")
public class BilltypeController {

    @Autowired
    private IBilltypeService iBilltypeService;

    @ResponseBody
    @RequestMapping("/list")
    public List<CcmsBilltype> geiAlltype(){
        return iBilltypeService.getAlltype();
    }
}
