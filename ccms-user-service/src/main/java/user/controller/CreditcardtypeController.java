package user.controller;

import com.alibaba.fastjson.JSON;
import entity.CcmsCreditcardtype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import user.service.ICreditCardTypeService;

import java.util.List;

@Controller
@RequestMapping("/creditType")
public class CreditcardtypeController {
    @Autowired
    private ICreditCardTypeService iService;



    //根据卡号来查询卡的等级和利率
    @RequestMapping("/selectInterestRate")
    @ResponseBody
    public String selectInterestRate(String cardnum){
        CcmsCreditcardtype creditcardtype = iService.selectInterestRate(cardnum);
        System.out.println("返回了");
        return JSON.toJSONString(creditcardtype);
    }

    @RequestMapping("/goCreditItem")
    public String goCreditItem(Model model) {
        model.addAttribute("CreditTypes", iService.getAllType());
        return "creditItem";
    }


    @RequestMapping("/getcardtype")
    @ResponseBody
    public List<CcmsCreditcardtype> goCreditType() {
        return iService.getCardtype();
    }
}
