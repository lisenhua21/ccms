package user.controller;

import entity.CcmsCredit;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.support.SessionStatus;
import user.service.ICredit;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@Api(tags = "跳转控制")
public class IndexController {

    @Resource
    ICredit iCredit;

    @GetMapping(value = "tologin")
    public String toLogin(){
        return "login";
    }

    @GetMapping(value = {"/","index"})
    public String index(){
        return "index";
    }

    @GetMapping(value = "toregister")
    public String toregister(){
        return "register";
    }

    @GetMapping(value = "index_v1")
    public String index_v1(){
        return "index_v1";
    }

    @GetMapping(value = "repaymentmenu")
    public String repaymentmenu(Model model,HttpServletRequest request){
        String uid = (String) request.getSession().getAttribute("userid");
        Integer userid= Integer.parseInt(uid);
        List<CcmsCredit> myCredit = iCredit.getMyCredit(userid);
        model.addAttribute("myCredit",myCredit);
        return "repaymentmenu";
    }

    @GetMapping(value = "exitToLogin")
    public String exitUser(HttpServletRequest request, SessionStatus sessionStatus){
        request.getSession().removeAttribute("userid");
        request.getSession().invalidate();
        sessionStatus.setComplete();
        return "login";
    }
}
