package user.controller;

import com.alibaba.fastjson.JSON;
import commons.ResultBean;
import entity.CcmsInstallment;
import entity.TemporaryInstallment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import user.service.IBillService;
import user.service.IInstallmentService;

/**
 * 申请分期模块
 * 李森华
 * 2012年3月4号
 */
@Controller
@RequestMapping("/installment/")
public class InstallmentController {


    @Autowired
    IInstallmentService installmentService;

    @Autowired
    IBillService billService;

  /*  @Autowired
    RedisTemplate redisTemplate;*/

    //通过账单查询分期申请信息
    //多表联查(账户表,基本信息表,账单表,信用卡表 )
    @RequestMapping("selectInstallment")
    public String selectbillByid(String billId, Model model) {
        TemporaryInstallment temporaryInstallment = installmentService.selectBill(billId);
        temporaryInstallment.getMaxInstallment();
        if (temporaryInstallment != null) {
            model.addAttribute("temporaryInstallment", temporaryInstallment);
            return "installment";
        }
        model.addAttribute("msg", "操作有误,请重新操作");
        return "installment";
    }

    //申请分期数据
    @PostMapping("insertInstallmentApply")
    @ResponseBody
    public String insertInstallmentApply(@RequestBody CcmsInstallment ccmsInstallment) {
        int i = installmentService.insertInstallmentApply(ccmsInstallment);
        if (i==1) {
            ResultBean resultBean = new ResultBean(1, "您的分期申请已经提交到工作人员审核,工作人员会在近期为您审核", 1);
            String jsonString = JSON.toJSONString(resultBean);
            return jsonString;
        }
        ResultBean resultBean = new ResultBean(1, "您的分期申请出现错误,很抱歉请您重新填写资料.如有问题请联系客服", 0);
        String jsonString = JSON.toJSONString(resultBean);
        return jsonString;
    }


}
