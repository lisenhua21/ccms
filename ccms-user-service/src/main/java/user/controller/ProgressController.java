package user.controller;

import commons.PageResult;
import entity.*;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;
import user.service.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 申请进度查询功能---1.办卡申请；2.分期申请
 * By WX
 */
@RestController
@RequestMapping("/progress")
@ApiOperation(value = "办卡进度查询")
public class ProgressController {
    //cardprogress表
    @Autowired
    ICardProgressService iCardProgressService;
    //t_essential表
    @Autowired
    ITEssential itEssential;
    //t_unitinfortion表
    @Autowired
    ITUnitinfortion itUnitinfortion;
    //t_contacts表
    @Autowired
    ITContacts itContacts;

    //转到信用卡申请查询页面
    @ApiIgnore
    @RequestMapping("/goCardProgress")
    public ModelAndView goCardProgress(){
        return new ModelAndView("cardprogress");
    }
    /**
     * 分页查询办卡申请列表
     */
    @ApiOperation(value = "分页查询办卡申请列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageIndex",value = "当前页码",required = false,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize",value = "页面大小",required = false,dataType = "Integer"),
            @ApiImplicitParam(name = "cpStatus",value = "申请状态",allowableValues = "成功,失败,申请中",required = false,dataType = "String"),
            @ApiImplicitParam(name = "startTime",value = "申请时间范围开始",required = false,dataType = "Date"),
            @ApiImplicitParam(name = "endTime",value = "申请时间范围结束",required = false,dataType = "Date")
    })

    @GetMapping("/getCardProgressList")
    public PageResult<CardProgress> getCardProgressList(Integer pageIndex, Integer pageSize, String cpStatus, String startTime, String endTime, HttpServletRequest request){
        Integer userId =  Integer.parseInt(request.getSession().getAttribute("userid").toString());
        pageIndex = pageIndex==null?1:pageIndex;
        pageSize = pageSize==null?5:pageSize;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date sTime = null;
        Date eTime = null;
        try {
            sTime = startTime==null?null:sdf.parse(startTime);
            eTime = endTime==null?null:new Date(sdf.parse(endTime).getTime()+24*60*60*1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return iCardProgressService.getCardProgressList(userId,pageIndex,pageSize,cpStatus,sTime,eTime);
    }
    /**
     * 查询基本信息
     */
    @ApiOperation(value = "根据基本信息表id查询基本信息")
    @ApiImplicitParam(name = "essentialId",value = "基本信息表id",required = true,dataType = "Integer")
    @GetMapping("/getEssential")
    public TEssential getEssential(Integer essentialId){
        return itEssential.getTEssential(essentialId);
    }
    /**
     * 查询单位信息
     */
    @ApiOperation(value = "根据单位信息表id查询基本信息")
    @ApiImplicitParam(name = "uid",value = "单位信息表id",required = true,dataType = "Integer")
    @GetMapping("/getUnit")
    public TUnitinfortion getUnit(Integer uid){
        return itUnitinfortion.getUnit(uid);
    }
    /**
     * 查询联系人信息
     */
    @ApiOperation(value = "根据联系人信息表id查询基本信息")
    @ApiImplicitParam(name = "contactId",value = "联系人 信息表id",required = true,dataType = "Integer")
    @GetMapping("/getContact")
    public TContacts getContact(Integer contactId){
        return itContacts.getContact(contactId);
    }

}