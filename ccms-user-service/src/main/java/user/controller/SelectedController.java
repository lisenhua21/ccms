package user.controller;

import entity.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import user.service.ISelectedService;

import java.util.List;


/**
 * <p>
 * 究极查询 只是一个selected
 * </p>
 *
 * @author liso
 * @since 2020-09-25
 */
@Api(tags = "查询selected需要用到的数据")
@Transactional
@RestController
@RequestMapping("/selected")
public class SelectedController {

    @Autowired
    private ISelectedService iSelectedService;

    @ApiOperation(value = "获取民族列表")
    @GetMapping("/getnation")
    public List<TNation> getnation() {

        return iSelectedService.getTNation();
    }

    @ApiOperation(value = "获取城市列表")
    @GetMapping("/getcity")
    public List<CcmsCity> getCity() {

        return iSelectedService.getCCity();
    }


    @ApiOperation(value = "获取职位列表")
    @GetMapping("/getposition")
    public List<TPosition> getposition() {
        return iSelectedService.getposition();
    }

    @ApiIgnore
    @GetMapping("/getessential")
    public List<TEssential> getTEssential() {

        return iSelectedService.getTEssential();
    }

    @ApiOperation(value = "获取婚姻状况列表")
    @GetMapping("/getmarital")
    public List<TMarital> getTMarital() {
        System.out.println("come");
        return iSelectedService.getTMarital();
    }

    @ApiOperation(value = "获取领卡方式列表")
    @GetMapping("/getmethod")
    public List<TMethod> getTMethod() {
        System.out.println("come");
        return iSelectedService.getTMethod();
    }

    @ApiIgnore
    @GetMapping("/getunitinfortion")
    public List<TUnitinfortion> getTUnitinfortion() {
        return iSelectedService.getTUnitinfortion();
    }

    @ApiOperation(value = "获取单位性质列表")
    @GetMapping("/getnature")
    public List<TUnitnature> getTUnitnature() {
        return iSelectedService.getTUnitnature();
    }

    @ApiOperation(value = "获取教育程度列表")
    @GetMapping("/getlevel")
    public List<TEdulv> getlevel() {
        return iSelectedService.getTEdulv();
    }

    @ApiOperation(value = "获取问题列表")
    @GetMapping("/getquestion")
    public List<TQuestion> getquestion() {
        return iSelectedService.getTQuestion();
    }

    @ApiOperation(value = "获取行业列表")
    @GetMapping("/getindustry")
    public List<TIndustry> getindustry() {
        return iSelectedService.getTIndustry();
    }

}
