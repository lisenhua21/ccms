package user.controller;

import com.aliyuncs.exceptions.ClientException;
import commons.MD5;
import commons.ResultBean;
import commons.SmsUtils;
import entity.CcmsUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import user.service.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
//import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/users")
@Api(tags = "登陆注册")
public class UserController {
    @Resource
    private UserService userService;

    @Resource
    RedisTemplate<String,Object> redisTemplate;

    /**
     * 用户账号密码登录
     * @param ccmsUser 用户
     * @return 返回登陆结果
     */
    @ApiOperation(value = "用户名密码登录")
    @ApiImplicitParam(name = "ccmsUser",value = "用户",required = true,dataType = "CcmsUser")
    @PostMapping(value = "loginpwd")
    @ResponseBody
    public ResultBean<CcmsUser> loginPwd(@RequestBody CcmsUser ccmsUser,HttpServletRequest request){
        MD5 md5 = new MD5();
        String pwd = md5.getMD5ofStr(ccmsUser.getUserPassword());
        CcmsUser user = userService.loginPwd(new CcmsUser(ccmsUser.getUserAccount(),pwd));
        if(user != null){
            redisTemplate.opsForValue().set("user"+user.getUserId(), user);
            String uid = String.valueOf(user.getUserId());
            request.getSession().setAttribute("userid",uid);
            request.getSession().setAttribute("user",user);
            return new ResultBean<>(200,"登陆成功",user);
        }
        else {
            return new ResultBean<>(-1,"登陆失败");
        }
    }

    /**
     * 用户手机号登录
     * @param phoneNumber 手机号
     * @return 返回登陆结果
     */
    @ApiOperation(value = "手机号登录")
    @ApiImplicitParam(name = "phoneNumber",value = "手机号",required = true,dataType = "String")
    @PostMapping(value = "loginByTel")
    public ResultBean<CcmsUser> loginByCode(String phoneNumber,HttpServletRequest request){
        CcmsUser user = userService.loginByPhone(phoneNumber);
        if (user != null){
            redisTemplate.opsForValue().set("user"+user.getUserId(), user);
            String uid = String.valueOf(user.getUserId());
            request.getSession().setAttribute("userid",uid);
            request.getSession().setAttribute("user",user);
            return new ResultBean<>(200,"登陆成功",user);
        }
        else{
            return new ResultBean<>(-1,"登陆失败,该手机用户不存在,请注册后登陆");
        }
    }

    /**
     * 用户注册
     * @param ccmsUser 用户
     * @return 返回注册结果
     */
    @ApiOperation(value = "注册")
    @ApiImplicitParam(name = "ccmsUser",value = "用户",required = true,dataType = "CcmsUser")
    @PostMapping(value = "register")
    @ResponseBody
    public ResultBean<CcmsUser> register(@RequestBody CcmsUser ccmsUser){
        MD5 md5 = new MD5();
        String upwd = md5.getMD5ofStr(ccmsUser.getUserPassword());
        CcmsUser user = new CcmsUser(ccmsUser.getUserAccount(),
                                     upwd,
                                     ccmsUser.getUserTel());
        int n = userService.register(user);
        if (n > 0){
            return new ResultBean<>(200, "注册成功", null);
        }
        else {
            return new ResultBean<>(-1, "注册失败", null);
        }
    }

    /*
    public String getUuid(){
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.replace("-","");
        return uuid;
    }
     */

    /**
     * 用户发送验证码
     * @param phoneNumber 手机号
     * @return 返回结果
     */
    @ApiOperation(value = "发送验证码")
    @ApiImplicitParam(name = "phoneNumber",value = "手机号",required = true,dataType = "String")
    @GetMapping(value = "smscode")
    public ResultBean<String> getCode(String phoneNumber){
        String code =Integer.toString ((int) ((Math.random()*9+1)*100000));
        try {
            SmsUtils.sendSms(phoneNumber,code);
        } catch (ClientException e) {
            e.printStackTrace();
        }
        redisTemplate.opsForValue().set(phoneNumber,code,300, TimeUnit.SECONDS);
        return new ResultBean<>(200,"短信验证码发送成功");
    }

    /**
     * 验证短信验证码
     * @param verifyCode 验证码
     * @param phoneNumber 手机号
     * @return 返回验证结果
     */
    @ApiOperation(value = "验证验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "verufyCode",value = "验证码",required = true,dataType = "String"),
            @ApiImplicitParam(name = "phoneNumber",value = "手机号",required = true,dataType = "String")
    })
    @GetMapping(value = "verify")
    public ResultBean<CcmsUser> verify(String verifyCode,String phoneNumber){
        String code = (String) redisTemplate.opsForValue().get(phoneNumber);
        if (verifyCode.equals(code)) return new ResultBean<>(200, "短信验证码验证成功");
        return new ResultBean<>(-1, "短信验证码验证失败");
    }

    /**
     * 修改密码
     * @param ccmsUser 用户
     * @return 返回修改结果
     */
    @ApiOperation(value = "修改密码")
    @ApiImplicitParam(name = "ccmsUser",value = "用户",required = true,dataType = "CcmsUser")
    @GetMapping(value = "modifyPwd")
    public ResultBean<CcmsUser> modifyPassword(CcmsUser ccmsUser){
        MD5 md5 = new MD5();
        String upwd = md5.getMD5ofStr(ccmsUser.getUserPassword());
        CcmsUser user = new CcmsUser(ccmsUser.getUserAccount(),upwd);
        int n = userService.modifyPwd(user);
        if (n > 0) return new ResultBean<>(200,"修改密码成功");
        else return new ResultBean<>(-1,"修改密码失败");
    }

    /**
     * 注册时检查用户名是否已存在
     * @param account 用户名
     * @return 返回检查结果
     */
    @ApiOperation(value = "注册时检查用户名是否已存在")
    @ApiImplicitParam(name = "account",value = "用户名",required = true,dataType = "String")
    @GetMapping(value = "checkAccount")
    public ResultBean<CcmsUser> checkAccount(String account){
        CcmsUser user = userService.checkAccount(account);
        if (user != null){
            return new ResultBean<>(-1,"该账号已存在");
        }
        else {
            return new ResultBean<>(100,"该账号可以使用");
        }
    }

    /**
     * 注册时检查手机号是否已被注册
     * @param tel 手机号
     * @return 返回检查结果
     */
    @ApiOperation(value = "注册时检查手机号是否已被注册")
    @ApiImplicitParam(name = "tel",value = "手机号",required = true,dataType = "String")
    @GetMapping(value = "checkTel")
    public ResultBean<CcmsUser> checkTel(String tel){
        CcmsUser user = userService.checkTel(tel);
        if (user != null){
            return new ResultBean<>(-1,"该手机号已被注册");
        }
        else {
            return new ResultBean<>(200,"该手机号可以使用");
        }
    }

    /**
     *  使用手机验证码修改密码
     * @param phoneNumber 手机号
     * @param vcode 验证码
     * @param pwd 密码
     * @return 返回修改结果
     */
    @ApiOperation(value = "使用手机验证码修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phoneNumber",value = "手机号",required = true,dataType = "String"),
            @ApiImplicitParam(name = "vcode",value = "验证码",required = true,dataType = "String"),
            @ApiImplicitParam(name = "pwd",value = "新的密码",required = true,dataType = "String")
    })
    @PostMapping(value = "modifyPwd")
    public ResultBean<CcmsUser> modifyPwd(String phoneNumber, String vcode, String pwd){
        String code = (String) redisTemplate.opsForValue().get(phoneNumber);
        MD5 md5 = new MD5();
        String password = md5.getMD5ofStr(pwd);
        if (vcode.equals(code)){
            int i = userService.modifyPwd(phoneNumber, password);
            if (i>0){
                return new ResultBean<>(200,"修改密码成功");
            }
            else {
                return new ResultBean<>(-1,"修改密码失败");
            }
        }
        return new ResultBean<>(-1,"短信验证码验证失败");
    }

}
