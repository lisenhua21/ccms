package user.mapper;

import entity.BillList;
import entity.CcmsBill;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Component
public interface BillMapper extends Mapper<CcmsBill> {
    @Select("SELECT b.`bill_id`,\n" +
            "b.`bill_create`,\n" +
            "(b.`bill_amount`+b.`bill_stage`) billmoney ,\n" +
            "b.`bill_status`,b.`bill_complete`,\n" +
            "cr.`cardnum`,b.`bill_repaytime`,\n" +
            "b.`bill_produce` FROM ccms_bill b , \n" +
            "ccms_creditcard cr \n" +
            "WHERE b.`credit_id`=cr.`credit_id` AND cr.`cardnum` LIKE #{cardnum}")
    List<BillList> getBillByCardnum(String cardnum);
}
