package user.mapper;

import entity.CcmsBilltype;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface BillTypeMapper extends Mapper<CcmsBilltype> {
}
