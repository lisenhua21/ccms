package user.mapper;

import entity.CardProgress;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.Date;
import java.util.List;

@Repository
public interface CardProgressMapper extends Mapper<CardProgress> {
    List<CardProgress> getCardProgressList(@Param("userId")Integer userId,@Param("cpStatus")String cpStatus,@Param("startTime")Date startTime,@Param("endTime")Date endTime);
}