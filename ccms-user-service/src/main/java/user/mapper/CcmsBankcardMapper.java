package user.mapper;


import entity.CcmsBankcard;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface CcmsBankcardMapper extends Mapper<CcmsBankcard> {

}
