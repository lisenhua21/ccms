package user.mapper;


import entity.CcmsRepayment;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface CcmsRepaymentMapper extends Mapper<CcmsRepayment> {
    @Insert("INSERT INTO ccms_repayment VALUE(NULL,'123456789012345',504.00,100.00,'9889455464656456456','2020-10-14 15:09:02','已还款',5,2),\n" +
            "(NULL,'123456789012345',504.00,100.00,'9889455464656456456','2020-10-14 15:09:02','已还款',5,2)")
    int add();
}
