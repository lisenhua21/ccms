package user.mapper;

import entity.CcmsBill;
import entity.CcmsCity;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface CityMapper extends Mapper<CcmsCity> {
}
