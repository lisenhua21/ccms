package user.mapper;

import entity.CcmsCredit;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Component
public interface CreditMapper extends Mapper<CcmsCredit> {
    List<CcmsCredit> selectAll(Example example);
}
