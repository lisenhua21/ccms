package user.mapper;

import entity.CcmsInstallment;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

import java.util.Date;
import java.util.List;

@Component
public interface InstallmentMapper extends Mapper<CcmsInstallment> {
    List<CcmsInstallment> getInstallment(@Param("statusId")Integer statusId, @Param("beginTime")Date beginTime, @Param("endTime")Date endTime);

    @Select("SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA. TABLES WHERE TABLE_NAME = 'ccms_installment'")
    public Integer selectNextId();


}
