package user.mapper;


import entity.TContacts;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface TContactsMapper extends Mapper<TContacts> {
    //查询联系人详情
    TContacts getContact(Integer contactId);
}