package user.mapper;

import entity.TEssential;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface TEssentialMapper extends Mapper<TEssential> {
    TEssential getTEssential(Integer essentialId);
}
