package user.mapper;


import entity.TIndustry;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;
@Component
public interface TIndustryMapper extends Mapper<TIndustry> {

}