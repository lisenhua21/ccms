package user.mapper;


import entity.TMarital;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface TMaritalMapper extends Mapper<TMarital> {

}