package user.mapper;


import entity.TMethod;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface TMethodMapper extends Mapper<TMethod> {

}