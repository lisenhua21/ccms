package user.mapper;



import entity.TNation;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface TNationMapper extends Mapper<TNation> {

}