package user.mapper;



import entity.TNation;
import entity.TPosition;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface TPositionMapper extends Mapper<TPosition> {

}