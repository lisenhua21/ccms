package user.mapper;


import entity.TQuestion;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;
@Component

public interface TQuestionMapper extends Mapper<TQuestion> {

}