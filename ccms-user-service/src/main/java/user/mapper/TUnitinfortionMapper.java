package user.mapper;


import entity.TUnitinfortion;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component

public interface TUnitinfortionMapper  extends Mapper<TUnitinfortion> {
    //查询单位信息详情
    TUnitinfortion getUnit(Integer uid);
}