package user.mapper;

import entity.TemporaryInstallment;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;
@Component
public interface TemporaryInstallmentMapper extends Mapper<TemporaryInstallment> {
    @Select("SELECT " +
            "c2.cardnum creditcardNumber," +
            "c2.credit_limit ccmsUserLimit," +
            "c4.user_creditrating ccmsUserCreditrating," +
            "c1.bill_create ccmsBillCreate ," +
            "c2.debt ccmsBillAmount ," +
            "c1.bill_id billId " +
            "FROM ccms_bill c1," +
            "ccms_creditcard c2," +
            "t_essential c3," +
            "ccms_user_info c4 " +
            "WHERE " +
            "c1.bill_id = 1 AND " +
            "c1.credit_id = c2.credit_id AND " +
            "c2.essentialId = c3.essentialId AND " +
            "c3.`user_id`=c4.user_id")
    TemporaryInstallment selectInstallment(String billId);
}
