package user.mapper;

import entity.CcmsUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Example;

@Component
public interface UserInfoMapper extends Mapper<CcmsUserInfo> {

    CcmsUserInfo updateByExample(Example example);

    String getUserPhoneById(Integer uId);
}
