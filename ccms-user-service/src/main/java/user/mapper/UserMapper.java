package user.mapper;

import entity.CcmsUser;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface UserMapper extends Mapper<CcmsUser> {

    @Insert("insert INTO ccms_user (user_id,user_name,user_account,user_password,user_phone) VALUES (REPLACE(UUID(),'-',''),'admin',123456,'123456',123456)")
    int register(CcmsUser ccmsUser);
}
