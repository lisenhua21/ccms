package user.service;

import entity.CcmsBankcard;

import java.util.List;

public interface IBankcardService {
    public List<CcmsBankcard> getAllBankcard();

    public CcmsBankcard getBankcard(String bankcardnum);

   public int deductmoney(CcmsBankcard bankcard);

    List<CcmsBankcard> getBankcardByUserId(Integer userId);
}
