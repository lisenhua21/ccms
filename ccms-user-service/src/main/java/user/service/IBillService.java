package user.service;

import entity.CcmsBill;

import java.math.BigDecimal;
import java.util.List;

public interface IBillService {
    List<CcmsBill> getAllBill(String creditId,String flag);

    CcmsBill getBill(String billCreate,String creitId);
    public CcmsBill getbillByPrimaryKey(int i);

    List<CcmsBill> getbillByCreditId(Integer creditId);

    int updateBill(Integer billId, BigDecimal money);

    int updateCreate(Integer creditId);

    int updateBillType(Integer billId);

    List<CcmsBill> getBillByDate(String nowTime);
}
