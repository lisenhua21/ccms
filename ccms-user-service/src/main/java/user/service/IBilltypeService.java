package user.service;

import entity.CcmsBilltype;

import java.util.List;

public interface IBilltypeService {
    List<CcmsBilltype> getAlltype();
}
