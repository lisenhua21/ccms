package user.service;

import commons.PageResult;
import entity.CardProgress;

import java.util.Date;


public interface ICardProgressService {
    PageResult<CardProgress> getCardProgressList(Integer userId,Integer pageIndex, Integer pageSize, String cpStatus, Date startTime, Date endTime);

    int insertProgress(CardProgress cardProgress);
}