package user.service;

import commons.ResultBean;
import entity.CcmsCredit;
import entity.CcmsUserInfo;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

public interface ICredit {
    public ResultBean toActive(CcmsCredit ccmsCredit);

    public ResultBean updateStatus(CcmsCredit ccmsCredit,int type);

    public CcmsCredit getCredit(long cardnum,HttpServletRequest request);

    List<CcmsCredit> getMyCredit(Integer userId);

    int updatepwd(CcmsCredit ccmsCredit);

    public CcmsCredit getCreditById(Integer creditid);

    CcmsCredit updateRemindTime(String remindTime, HttpServletRequest request);

    int updatedebt(CcmsCredit credit);

}
