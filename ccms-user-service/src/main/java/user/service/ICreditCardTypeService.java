package user.service;

import commons.ResultBean;
import entity.CcmsCreditcardtype;

import java.util.List;

public interface ICreditCardTypeService {
    ResultBean<CcmsCreditcardtype> getAllType();

    CcmsCreditcardtype getType(String typename);

    CcmsCreditcardtype getTypeById(Integer typeId);

    List<CcmsCreditcardtype> getCardtype();
    //根据卡号查询等级和利率
    CcmsCreditcardtype selectInterestRate(String cardnum);
}
