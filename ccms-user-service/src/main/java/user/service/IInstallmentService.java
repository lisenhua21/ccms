package user.service;

import entity.CcmsInstallment;
import entity.TemporaryInstallment;

/**
 * 申请分期Service
 * 李森华
 */
public interface IInstallmentService {
    //查询账单数据
    TemporaryInstallment selectBill(String billId);

    int insertInstallmentApply(CcmsInstallment ccmsInstallment);

    Integer selectNextId();

}
