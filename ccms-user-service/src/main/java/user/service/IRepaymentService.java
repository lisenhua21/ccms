package user.service;



import commons.PageResult;
import entity.CcmsRepayment;

import java.util.List;

public interface IRepaymentService {
    public List<CcmsRepayment> getAllRepayment();

   public int addRepayment(CcmsRepayment repayment);

   public PageResult getpage(int pageIndex, int pageSize, String userId);
}
