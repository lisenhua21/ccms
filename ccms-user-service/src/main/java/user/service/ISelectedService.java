package user.service;

import entity.*;

import java.util.List;

public interface ISelectedService {


    public List<TNation> getTNation();

    public List<CcmsCity> getCCity();

    public List<TMarital> getTMarital();

    public List<TEssential> getTEssential();

    public List<TMethod> getTMethod();

    public List<TUnitinfortion> getTUnitinfortion();

    public List<TUnitnature> getTUnitnature();

    public List<TEdulv> getTEdulv();

    public List<TIndustry> getTIndustry();

    public List<TQuestion> getTQuestion();

    List<TPosition> getposition();

    List<CcmsCity> editCity();
}
