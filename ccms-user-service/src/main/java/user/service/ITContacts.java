package user.service;

import entity.TContacts;

public interface ITContacts {
    //查询联系人详情
    TContacts getContact(Integer contactId);
}
