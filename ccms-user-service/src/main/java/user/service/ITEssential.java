package user.service;

import commons.ResultBean;
import entity.TEssential;

public interface ITEssential {
    public TEssential getUser(String identityId);

    public TEssential getUserByKey(int userId);

    TEssential getEssentialByUserId(Integer uId);

    TEssential getTEssential(Integer essentialId);

    ResultBean updateInformation(Integer uId, Integer cityid, String house, Integer postnum, Integer essentialid);
}
