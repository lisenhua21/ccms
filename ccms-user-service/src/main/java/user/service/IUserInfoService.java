package user.service;

import commons.ResultBean;
import entity.CcmsUserInfo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface IUserInfoService {
    CcmsUserInfo getUserById(Integer userId);

    CcmsUserInfo updateUser(String billType, HttpServletRequest request);

    CcmsUserInfo updateAuto(HttpServletRequest request);

    List<CcmsUserInfo> getAllUser();

    List<CcmsUserInfo> getUserByDate(String date);

    String getUserPhoneById(Integer uId);

    int updateUserPhone(Integer uId, String phoneNum);

    ResultBean editPwd(String userid, String oldPwd, String newPwd);
}
