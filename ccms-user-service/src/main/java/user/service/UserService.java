package user.service;

import entity.CcmsUser;

import java.util.List;

public interface UserService {
    CcmsUser loginPwd(CcmsUser ccmsUser);

    int register(CcmsUser ccmsUser);

    CcmsUser loginByPhone(String phoneNumber);

    int modifyPwd(CcmsUser user);

    CcmsUser checkAccount(String account);

    CcmsUser checkTel(String tel);

    int modifyPwd(String phoneNumber, String pwd);

    List<CcmsUser> getAllUser();
}
