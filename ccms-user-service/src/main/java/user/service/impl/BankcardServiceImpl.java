package user.service.impl;


import entity.CcmsBankcard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import user.mapper.CcmsBankcardMapper;
import user.service.IBankcardService;

import java.util.List;

@Service
public class BankcardServiceImpl implements IBankcardService {
    @Autowired
    CcmsBankcardMapper bankcardMapper;


    @Override
    public List<CcmsBankcard> getAllBankcard() {
        return bankcardMapper.selectAll();
    }

    @Override
    public CcmsBankcard getBankcard(String bankcardnum) {
        CcmsBankcard bankcard = null;
        Example example = new Example(CcmsBankcard.class);
        Example.Criteria criteria = example.createCriteria();
        if (bankcardnum != null && !bankcardnum.equals("")) {
            criteria.andEqualTo("bankcardnum", bankcardnum);
            List<CcmsBankcard> ccmsBankcards = bankcardMapper.selectByExample(example);
            bankcard = ccmsBankcards.get(0);
        }
        return bankcard;
    }

    @Override
    public int deductmoney(CcmsBankcard bankcard) {
        System.out.println("惊了");
        int n = bankcardMapper.updateByPrimaryKey(bankcard);
        System.out.println(n);
        return n;
    }

    @Override
    public List<CcmsBankcard> getBankcardByUserId(Integer userId) {

        Example example = new Example(CcmsBankcard.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userId", userId);
        List<CcmsBankcard> bankcards = bankcardMapper.selectByExample(example);
        return bankcards;
    }
}
