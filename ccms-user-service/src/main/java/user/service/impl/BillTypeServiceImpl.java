package user.service.impl;

import entity.CcmsBilltype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import user.mapper.BillTypeMapper;
import user.service.IBilltypeService;
import java.util.List;

@Service
public class BillTypeServiceImpl implements IBilltypeService {

    @Autowired
    private BillTypeMapper mapper;

    @Override
    public List<CcmsBilltype> getAlltype() {
        return mapper.selectAll();
    }
}
