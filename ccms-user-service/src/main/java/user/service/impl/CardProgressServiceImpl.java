package user.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import commons.PageResult;
import entity.CardProgress;
import entity.CcmsBankcard;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;
import tk.mybatis.mapper.entity.Example;
import user.mapper.CardProgressMapper;
import user.service.ICardProgressService;

import java.util.Date;

@Service
public class CardProgressServiceImpl implements ICardProgressService {
    @Autowired
    CardProgressMapper cardProgressMapper;




    public PageResult<CardProgress> getCardProgressList(Integer userId,Integer pageIndex, Integer pageSize, String
            cpStatus, Date startTime, Date endTime) {
        PageHelper.startPage(pageIndex, pageSize);
        Page<CardProgress> page = (Page<CardProgress>) cardProgressMapper.getCardProgressList(userId,cpStatus, startTime, endTime);
        return new PageResult<>(
                page.getPageNum(),
                page.getPageSize(),
                page.getTotal(),
                page.getPages(),
                page.getResult());
    }

    @Override
    public int insertProgress(CardProgress cardProgress) {

        return cardProgressMapper.insert(cardProgress);
    }
}