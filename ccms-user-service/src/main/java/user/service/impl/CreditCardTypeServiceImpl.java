package user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import commons.ResultBean;
import entity.CcmsCreditcardtype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import user.mapper.CreditCardTpyeMapper;
import user.service.ICreditCardTypeService;

import java.util.List;

@Service
public class CreditCardTypeServiceImpl implements ICreditCardTypeService {

    @Autowired
    private CreditCardTpyeMapper mapper;

    @Autowired
    private StringRedisTemplate template;

    @Override
    public ResultBean<CcmsCreditcardtype> getAllType() {
        String creditTpye = template.opsForValue().get("creditTpye");
        List<CcmsCreditcardtype> CreditTypes = null;
        if (creditTpye == null) {
            CreditTypes = mapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(CreditTypes);
            template.opsForValue().set("creditTpye", jsonCreditTypes);
        } else {
            CreditTypes = JSONArray.parseArray(creditTpye, CcmsCreditcardtype.class);
        }
        return new ResultBean<>(0, "查询成功!!!", CreditTypes);
    }

    @Override
    public List<CcmsCreditcardtype> getCardtype() {
        String creditTpye = template.opsForValue().get("creditTpye1");
        List<CcmsCreditcardtype> creditTypes = null;
        if (creditTpye == null) {
            creditTypes = mapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(creditTypes);
            template.opsForValue().set("creditTpye1", jsonCreditTypes);
        } else {
            creditTypes = JSONArray.parseArray(creditTpye, CcmsCreditcardtype.class);
        }
        return creditTypes;
    }

    //根据卡号查询该卡等级利率
    @Override
    public CcmsCreditcardtype selectInterestRate(String cardnum) {
        CcmsCreditcardtype creditcardtype = mapper.selectInterestRate(cardnum);
        return creditcardtype;
    }

    @Override
    public CcmsCreditcardtype getType(String typename) {
        Example example = new Example(CcmsCreditcardtype.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("typename", typename);
        return mapper.selectOneByExample(example);
    }

    @Override
    public CcmsCreditcardtype getTypeById(Integer typeId) {
        return mapper.selectByPrimaryKey(typeId);

    }
}
