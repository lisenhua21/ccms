package user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import commons.ResultBean;
import entity.CcmsCredit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import user.mapper.CreditMapper;
import user.service.ICredit;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

@Service
public class CreditImlp  implements ICredit {

    @Autowired
    CreditMapper creditMapper;

    @Autowired
    private StringRedisTemplate template;

    //激活信用卡功能
    @Override
    public ResultBean toActive(CcmsCredit ccmsCredit) {
        ccmsCredit.setActive("待激活");
        int s=creditMapper.updateByPrimaryKey(ccmsCredit);
        if(s>0){
            return new ResultBean(200,"申请激活中，请静待2~3个工作日！！",ccmsCredit);
        }else {
            return null;
        }
    }

    //修改信用卡状态(挂失，冻结，注销，解冻，解除挂失)
    @Override
    public ResultBean updateStatus(CcmsCredit ccmsCredit, int type) {
        switch (type){
            case 1:
                ccmsCredit.setStatus("正常");
                break;
            case 2:
                ccmsCredit.setStatus("申请挂失");
                break;
            case 3:
                ccmsCredit.setStatus("已挂失");
                break;
            case 4:
                ccmsCredit.setStatus("申请冻结");
                break;
            case 5:
                ccmsCredit.setStatus("已冻结");
                break;
            case 6:
                ccmsCredit.setStatus("申请注销");
                break;
            case 7:
                ccmsCredit.setStatus("已注销");
                break;
        }
        int s=creditMapper.updateByPrimaryKey(ccmsCredit);
        if(s>0){
            return new ResultBean(200,"修改成功",ccmsCredit);
        }else {
            return new ResultBean(500,"修改失败",ccmsCredit);
        }
    }

    //通过卡号查询信用卡
    @Override
    public CcmsCredit getCredit(long cardnum,HttpServletRequest request) {
        Integer userId =  Integer.parseInt(request.getSession().getAttribute("userid").toString());
        Example example=new Example(CcmsCredit.class);
        Example.Criteria criteria=example.createCriteria();
        criteria.andEqualTo("cardnum",cardnum);
        CcmsCredit ccmsCredit = creditMapper.selectOneByExample(example);
        template.opsForValue().getAndSet("creditId"+userId,ccmsCredit.getCreditId()+"");
        return ccmsCredit;
    }

    @Override
    //通过userId查询所有的卡
    public List<CcmsCredit> getMyCredit(Integer userId) {
        String userCredits = template.opsForValue().get("userCredits"+userId);
        List<CcmsCredit> ccmsCredits = null;
        if (userCredits == null){
            Example example = new Example(CcmsCredit.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("userId",userId);
            ccmsCredits = creditMapper.selectAll(example);
            template.opsForValue().set("userCredits"+userId, JSON.toJSONString(ccmsCredits));
        }else {
            ccmsCredits = JSONArray.parseArray(userCredits,CcmsCredit.class);
            System.out.println("ccmsCredits = " + ccmsCredits.toString());
        }
        return ccmsCredits;
    }

    @Override
    public int updatepwd(CcmsCredit ccmsCredit) {
        return creditMapper.updateByPrimaryKey(ccmsCredit);
    }


    @Override
    public CcmsCredit updateRemindTime(String remindTime, HttpServletRequest request) {
        Integer userId =  Integer.parseInt((String)request.getSession().getAttribute("userid"));
        String creditId = template.opsForValue().get("creditId"+userId);
        CcmsCredit ccmsCredit = creditMapper.selectByPrimaryKey(creditId);
        String time = ccmsCredit.getRemindTime();
        if (remindTime == null || remindTime.equals("") || remindTime.equals("null")){
            remindTime = time;
            System.out.println("remindTime = " + remindTime);
        }
        ccmsCredit.setRemindTime(remindTime);
        creditMapper.updateByPrimaryKeySelective(ccmsCredit);
        return ccmsCredit;
    }

    //还款成功后修改欠款金额
    @Override
    public int updatedebt(CcmsCredit credit) {
        return creditMapper.updateByPrimaryKey(credit);
    }

    @Override
    public CcmsCredit getCreditById(Integer creditid) {
        return creditMapper.selectByPrimaryKey(creditid);
    }
}
