package user.service.impl;

import entity.TContacts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import user.mapper.TContactsMapper;
import user.service.ITContacts;

@Service
public class ITContactsImpl implements ITContacts {
    @Autowired
    TContactsMapper tContactsMapper;
    @Override
    public TContacts getContact(Integer contactId) {
        return tContactsMapper.getContact(contactId);
    }
}