package user.service.impl;

import entity.TUnitinfortion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import user.mapper.TUnitinfortionMapper;
import user.service.ITUnitinfortion;

@Service
public class ITUnitinfortionImpl implements ITUnitinfortion {
    @Autowired
    TUnitinfortionMapper tUnitinfortionMapper;
    @Override
    public TUnitinfortion getUnit(Integer uid) {
        return tUnitinfortionMapper.getUnit(uid);
    }
}
