package user.service.impl;

import entity.CcmsBill;
import entity.CcmsInstallment;
import entity.CcmsStage;
import entity.TemporaryInstallment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import user.mapper.BillMapper;
import user.mapper.CcmsStageMapper;
import user.mapper.InstallmentMapper;
import user.mapper.TemporaryInstallmentMapper;
import user.service.IInstallmentService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 申请分期Service
 * 李森华
 */
@Service
public class InstallmentServiceImpl implements IInstallmentService {
    @Autowired
    InstallmentMapper installmentMapper;
    @Autowired
    BillMapper billMapper;
    @Autowired
    CcmsStageMapper ccmsStageMapper;
    @Autowired
    TemporaryInstallmentMapper temporaryInstallmentMapper;

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 李森华     * 申请分期需要的资料
     *
     * @param billId
     * @return
     */
    @Override
    public TemporaryInstallment selectBill(String billId) {
        TemporaryInstallment temporaryInstallment = temporaryInstallmentMapper.selectInstallment(billId);
        return temporaryInstallment;
    }

    @Override
    @Transactional    //开启事务
    public int insertInstallmentApply(CcmsInstallment ccmsInstallment) {
        Date date = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
        String format = ft.format(date);
        ccmsInstallment.setApplyDate(format);
        int insert = installmentMapper.insert(ccmsInstallment);
        Integer installmentId = ccmsInstallment.getInstallmentId();
        if (insert >= 1) {
            //修改账单状态
            int i1 = billMapper.updateByPrimaryKeySelective(new CcmsBill(ccmsInstallment.getBillId(), "已分期"));
            if (i1>=1){
                CcmsBill ccmsBill = billMapper.selectByPrimaryKey(ccmsInstallment.getBillId());
                String billCreate = ccmsBill.getBillCreate();
//            把账单时间获取
                //用正则表达式
                String reg = "[^0-9]";
                //Pattern类的作用在于编译正则表达式后创建一个匹配模式.
                Pattern p = Pattern.compile(reg);
                //Matcher类使用Pattern实例提供的模式信息对正则表达式进行匹配
                Matcher m = p.matcher(billCreate);
                String mydate = m.replaceAll("").trim();
                //截取字符串前4位
                String year= mydate.substring(0,4);
                System.out.println("年"+year);
                //截掉字符串前4位
                String month = mydate.substring(4);
                System.out.println("月"+month);
                //年份
                int year2 = Integer.parseInt(year);
                //月份
                int month2 = Integer.parseInt(month);
                int time = ccmsInstallment.getInstallmentDateTime();
                for (int i = 1;i <= time;i++){
                    String num = i+"/"+time;
                    if (month2>=12){
                        month2 = 1;
                        year2+=1;
                    }else{
                        month2+=1;
                    }
                    String newdate = year2+"-"+month2;
                    CcmsStage ccmsStage = new CcmsStage(num,ccmsInstallment.getFirstMonth(),"未还",newdate,"",ccmsInstallment.getBillId(),installmentId);
                    ccmsStageMapper.insert(ccmsStage);
                }
                return 1;
            }
        }
        return 0;
    }

    @Override
    public Integer selectNextId() {
        return installmentMapper.selectNextId();
    }

    public static void main(String[] args) {
        Date date =new Date();
        System.out.println(date);
    }


}
