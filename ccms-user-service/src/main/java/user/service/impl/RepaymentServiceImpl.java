package user.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import commons.PageResult;
import entity.CcmsRepayment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import user.mapper.CcmsRepaymentMapper;
import user.service.IRepaymentService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

@Service
public class RepaymentServiceImpl implements IRepaymentService {
    @Autowired
    CcmsRepaymentMapper repaymentMapper;
    @Override
    public List<CcmsRepayment> getAllRepayment() {
        return repaymentMapper.selectAll();
    }

    @Override
    public int addRepayment(CcmsRepayment repayment) {
        int n=0;
        if (repayment.getCardnum()!=null&&repayment.getBankcardnum()!=null){
            n = repaymentMapper.insert(repayment);
        }
        return n;
    }
//根据userid查询还款明细返回pagebean
    @Override
    public PageResult getpage(int pageIndex, int pageSize, String userId) {
        PageHelper.startPage(pageIndex,pageSize);
        List<CcmsRepayment> list=null;
        if(userId!=null&&!userId.equals("")) {
            Example example = new Example(CcmsRepayment.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("userId", userId);
           example.setOrderByClause("repayment_date desc");
            list = repaymentMapper.selectByExample(example);
        }
        Page<CcmsRepayment> plist = (Page<CcmsRepayment>) list;
        PageResult pb=new PageResult(plist.getPageNum(),plist.getPageSize(),plist.getTotal(),plist.getPages(),plist.getResult());
        return pb;
    }
}

