package user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import user.mapper.*;
import user.service.ISelectedService;

import java.util.List;


@Service
public class SelectedServiceImpl implements ISelectedService {

    @Autowired
    private TNationMapper tNationMapper;
    @Autowired
    private TEdulvMapper tEdulvMapper;
    @Autowired
    private TMaritalMapper tMaritalMapper;
    @Autowired
    private TMethodMapper tMethodMapper;
    @Autowired
    private TEssentialMapper tEssentialMapper;
    @Autowired
    private TUnitinfortionMapper tUnitinfortionMapper;
    @Autowired
    private TUnitnatureMapper tUnitnatureMapper;
    @Autowired
    private TIndustryMapper tIndustryMapper;
    @Autowired
    private TQuestionMapper tQuestionMapper;
    @Autowired
    private CityMapper cityMapper;
    @Autowired
    private StringRedisTemplate template;

    @Override
    public List<TNation> getTNation() {
        String tNation = template.opsForValue().get("TNation");
        List<TNation> tNations = null;
        if (tNation == null) {
            tNations = tNationMapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(tNations);
            template.opsForValue().set("TNation", jsonCreditTypes);
        } else {
            tNations = JSONArray.parseArray(tNation, TNation.class);
        }


        return tNations;
    }

    @Override
    public List<CcmsCity> getCCity() {
        String tNation = template.opsForValue().get("CcmsCity");
        List<CcmsCity> citys = null;
        if (tNation == null) {
            Example example = new Example(CcmsCity.class);
            Example.Criteria criteri = example.createCriteria();
            criteri.andNotEqualTo("fid", 0);
            citys = cityMapper.selectByExample(example);
            String jsonCreditTypes = JSON.toJSONString(citys);
            template.opsForValue().set("CcmsCity", jsonCreditTypes);
        } else {
            citys = JSONArray.parseArray(tNation, CcmsCity.class);
        }


        return citys;
    }

    @Override
    public List<TMarital> getTMarital() {

        String tMarital = template.opsForValue().get("TMarital");
        List<TMarital> tMaritals = null;
        if (tMarital == null) {
            tMaritals = tMaritalMapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(tMaritals);
            template.opsForValue().set("TMarital", jsonCreditTypes);
        } else {
            tMaritals = JSONArray.parseArray(tMarital, TMarital.class);
        }
        return tMaritals;
    }

    @Override
    public List<TEssential> getTEssential() {
        String tEssential = template.opsForValue().get("TEssential");
        List<TEssential> tEssentials = null;
        if (tEssential == null) {
            tEssentials = tEssentialMapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(tEssentials);
            template.opsForValue().set("TEssential", jsonCreditTypes);
        } else {
            tEssentials = JSONArray.parseArray(tEssential, TEssential.class);
        }

        return tEssentials;
    }

    @Override
    public List<TMethod> getTMethod() {
        String tNation = template.opsForValue().get("TMethod");
        List<TMethod> tMethods = null;
        if (tNation == null) {
            tMethods = tMethodMapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(tMethods);
            template.opsForValue().set("TMethod", jsonCreditTypes);
        } else {
            tMethods = JSONArray.parseArray(tNation, TMethod.class);
        }

        return tMethods;

    }

    @Override
    public List<TUnitinfortion> getTUnitinfortion() {
        String tNation = template.opsForValue().get("TUnitinfortion");
        List<TUnitinfortion> tUnitinfortions = null;
        if (tNation == null) {
            tUnitinfortions = tUnitinfortionMapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(tUnitinfortions);
            template.opsForValue().set("TUnitinfortion", jsonCreditTypes);
        } else {
            tUnitinfortions = JSONArray.parseArray(tNation, TUnitinfortion.class);
        }
        return tUnitinfortions;
    }

    @Override
    public List<TUnitnature> getTUnitnature() {
        String tNation = template.opsForValue().get("TUnitnature");
        List<TUnitnature> tUnitnatures = null;
        if (tNation == null)

        {
            tUnitnatures = tUnitnatureMapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(tUnitnatures);
            template.opsForValue().set("TUnitnature", jsonCreditTypes);
        } else

        {
            tUnitnatures = JSONArray.parseArray(tNation, TUnitnature.class);
        }

        return tUnitnatures;
    }


    @Override
    public List<TEdulv> getTEdulv() {
        String tNation = template.opsForValue().get("TEdulv");
        List<TEdulv> tEdulvs = null;
        if (tNation == null) {
            tEdulvs = tEdulvMapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(tEdulvs);
            template.opsForValue().set("TEdulv", jsonCreditTypes);
        } else {
            tEdulvs = JSONArray.parseArray(tNation, TEdulv.class);
        }

        return tEdulvs;
    }

    @Override
    public List<TIndustry> getTIndustry() {

        String tNation = template.opsForValue().get("TIndustry");
        List<TIndustry> tIndustries = null;
        if (tNation == null)

        {
            tIndustries = tIndustryMapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(tIndustries);
            template.opsForValue().set("TIndustry", jsonCreditTypes);
        } else

        {
            tIndustries = JSONArray.parseArray(tNation, TIndustry.class);
        }

        return tIndustries;
    }

    @Override
    public List<TQuestion> getTQuestion() {
        String tNation = template.opsForValue().get("TQuestion");
        List<TQuestion> tQuestions = null;
        if (tNation == null)

        {
            tQuestions = tQuestionMapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(tQuestions);
            template.opsForValue().set("TQuestion", jsonCreditTypes);
        } else

        {
            tQuestions = JSONArray.parseArray(tNation, TQuestion.class);
        }

        return tQuestions;
    }

    @Autowired
    private TPositionMapper tPositionMapper;

    @Override
    public List<TPosition> getposition() {
        String tNation = template.opsForValue().get("TPosition");
        List<TPosition> tPositions = null;
        if (tNation == null)

        {
            tPositions = tPositionMapper.selectAll();
            String jsonCreditTypes = JSON.toJSONString(tPositions);
            template.opsForValue().set("TPosition", jsonCreditTypes);
        } else

        {
            tPositions = JSONArray.parseArray(tNation, TPosition.class);
        }

        return tPositions;
    }

    @Override
    public List<CcmsCity> editCity() {
        return cityMapper.selectAll();
    }


}
