package user.service.impl;

import commons.ResultBean;
import entity.TEssential;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import user.mapper.TEssentialMapper;
import user.service.ITEssential;

import java.util.LinkedList;
import java.util.List;

@Service
public class TEssentialImlp implements ITEssential {
    @Autowired
    TEssentialMapper tEssentialMapper;


    @Override
    public TEssential getUser(String identityId) {
        Example example=new Example(TEssential.class);
        Example.Criteria criteria=example.createCriteria();
        criteria.andEqualTo("identityid",identityId);
        return tEssentialMapper.selectOneByExample(example);
    }

    @Override
    public TEssential getUserByKey(int userId) {
        return tEssentialMapper.selectByPrimaryKey(userId);
    }

    @Override
    public TEssential getEssentialByUserId(Integer uId) {
        Example example = new Example(TEssential.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userId",uId);
        List<TEssential> list = tEssentialMapper.selectByExample(example);
        //TEssential tEssential = tEssentialMapper.selectOneByExample(example);
        return list.get(0);
    }

    @Override
    public TEssential getTEssential(Integer essentialId) {
        return tEssentialMapper.getTEssential(essentialId);
    }

    @Override
    public ResultBean updateInformation(Integer uId, Integer cityid, String house, Integer postnum, Integer essentialid) {
        TEssential tEssential = new TEssential();
        tEssential.setEssentialid(essentialid);
        tEssential.setCityid(cityid);
        tEssential.setHouse(house);
        tEssential.setPostnum(postnum);
        int update = tEssentialMapper.updateByPrimaryKeySelective(tEssential);
        ResultBean resultBean = new ResultBean();
        if (update > 0){
            resultBean.setCode(200);
            resultBean.setMessage("修改成功");
        }else {
            resultBean.setCode(-1);
            resultBean.setMessage("修改失败");
        }
        return resultBean;
    }

}
