package user.service.impl;

import commons.ResultBean;
import entity.CcmsUser;
import entity.CcmsUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import tk.mybatis.mapper.entity.Example;
import user.mapper.UserInfoMapper;
import user.service.IUserInfoService;

import java.util.List;


import javax.servlet.http.HttpServletRequest;

@Service
public class UserInfoServiceImpl implements IUserInfoService {

    @Autowired
    private UserInfoMapper mapper;

    @Autowired
    private StringRedisTemplate template;

    @Override
    public CcmsUserInfo getUserById(Integer userId) {
        return mapper.selectByPrimaryKey(userId);
    }

    @Override
    public CcmsUserInfo updateUser(String billType, HttpServletRequest request) {
        String userId = (String) request.getSession().getAttribute("userid");
        int i = mapper.updateByPrimaryKeySelective(new CcmsUserInfo(Integer.valueOf(userId),billType));
        CcmsUserInfo ccmsUserInfo = mapper.selectByPrimaryKey(userId);
        return ccmsUserInfo;
    }

    @Override
    public CcmsUserInfo updateAuto(HttpServletRequest request) {
        String userId = (String) request.getSession().getAttribute("userid");
        CcmsUserInfo ccmsUserInfo = mapper.selectByPrimaryKey(userId);
        if (ccmsUserInfo.getAutoRepayment().equals("已开通")){
            ccmsUserInfo.setAutoRepayment("未开通");
        }else {
            ccmsUserInfo.setAutoRepayment("已开通");
        }
        mapper.updateByPrimaryKeySelective(ccmsUserInfo);
        return ccmsUserInfo;
    }

    @Override
    public List<CcmsUserInfo> getAllUser() {
        return mapper.selectAll();
    }

    @Override
    public List<CcmsUserInfo> getUserByDate(String date) {
        Example example = new Example(CcmsUserInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("repayment",date);
        List<CcmsUserInfo> ccmsUserInfos = mapper.selectByExample(example);
        return ccmsUserInfos;
    }

    @Override
    public String getUserPhoneById(Integer uId) {
        return mapper.getUserPhoneById(uId);
    }

    @Override
    public int updateUserPhone(Integer uId, String phoneNum) {
        CcmsUserInfo userInfo = new CcmsUserInfo();
        userInfo.setUserId(uId);
        userInfo.setUserTel(phoneNum);
        int result = mapper.updateByPrimaryKeySelective(userInfo);
        return result;
    }

    @Override
    public ResultBean editPwd(String userid, String oldPwd, String newPwd) {
        String md5OldWord = DigestUtils.md5DigestAsHex(oldPwd.getBytes());
        CcmsUserInfo userInfo = new CcmsUserInfo();
        userInfo.setUserId(Integer.valueOf(userid));
        CcmsUserInfo info = mapper.selectOne(userInfo);
        ResultBean resultBean = new ResultBean();
        if (md5OldWord.equalsIgnoreCase(info.getUserPassword())){
            String md5NewWord = DigestUtils.md5DigestAsHex(newPwd.getBytes());
            userInfo.setUserPassword(md5NewWord);
            int update = mapper.updateByPrimaryKeySelective(userInfo);
            if (update > 0){
                resultBean.setCode(200);
                resultBean.setMessage("密码修改成功");
            }else {
                resultBean.setCode(-1);
                resultBean.setMessage("密码修改失败");
            }
        }else {
            resultBean.setCode(-1);
            resultBean.setMessage("旧的密码错误,修改失败");
        }

        return resultBean;
    }

}
