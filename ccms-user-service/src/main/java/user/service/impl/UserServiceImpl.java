package user.service.impl;

import entity.CcmsUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import user.mapper.UserMapper;
import user.service.UserService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;
    @Override
    public CcmsUser loginPwd(CcmsUser ccmsUser) {
        return userMapper.selectOne(ccmsUser);
    }

    @Override
    public int register(CcmsUser ccmsUser) {
        return userMapper.insertSelective(ccmsUser);
    }

    @Override
    public CcmsUser loginByPhone(String phoneNumber) {
        return userMapper.selectOne(new CcmsUser(phoneNumber));
    }

    @Override
    public int modifyPwd(CcmsUser ccmsUser) {
        Example example = new Example(CcmsUser.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userAccount",ccmsUser.getUserAccount());
        criteria.andEqualTo("userPassword",ccmsUser.getUserPassword());
        return userMapper.updateByExample(ccmsUser,example);
    }

    @Override
    public CcmsUser checkAccount(String account) {
        Example example = new Example(CcmsUser.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userAccount",account);
        return userMapper.selectOneByExample(example);
    }

    @Override
    public CcmsUser checkTel(String tel) {
        Example example = new Example(CcmsUser.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userTel",tel);
        return userMapper.selectOneByExample(example);
    }

    @Override
    public int modifyPwd(String phoneNumber, String pwd) {
        Example example = new Example(CcmsUser.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userTel",phoneNumber);
        CcmsUser user = new CcmsUser();
        user.setUserPassword(pwd);
        return userMapper.updateByExampleSelective(user,example);
    }

    //查询所有用户
    @Override
    public List<CcmsUser> getAllUser() {
        return userMapper.selectAll();
    }

}
